-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 20 Mei 2019 pada 16.26
-- Versi Server: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mudaris_db`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `a_media`
--

CREATE TABLE `a_media` (
  `id` int(11) NOT NULL,
  `b_user_id` int(11) DEFAULT NULL,
  `utype` enum('blog','produk','produk_thumb','galeri') NOT NULL DEFAULT 'blog',
  `nama` varchar(255) NOT NULL,
  `folder` varchar(255) NOT NULL DEFAULT '/',
  `cdate` datetime NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `a_modules`
--

CREATE TABLE `a_modules` (
  `identifier` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `path` varchar(255) DEFAULT '',
  `level` int(1) NOT NULL DEFAULT '0' COMMENT 'depth level of menu, 0 mean outer 3 deeper submenu',
  `has_submenu` int(1) NOT NULL DEFAULT '0' COMMENT '1 mempunyai submenu, 2 tidak mempunyai submenu',
  `children_identifier` varchar(255) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `is_default` enum('allowed','denied') NOT NULL DEFAULT 'denied',
  `is_visible` int(1) NOT NULL DEFAULT '1',
  `priority` int(3) NOT NULL DEFAULT '0' COMMENT '0 mean higher 999 lower',
  `fa_icon` varchar(255) NOT NULL DEFAULT 'fa fa-home' COMMENT 'font-awesome icon on menu',
  `utype` varchar(48) NOT NULL DEFAULT 'internal' COMMENT 'type module : internal, external'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='list modul yang ada dimenu atau tidak ada dimenu';

-- --------------------------------------------------------

--
-- Struktur dari tabel `a_pengguna`
--

CREATE TABLE `a_pengguna` (
  `id` int(11) NOT NULL,
  `a_company_id` int(11) DEFAULT NULL COMMENT 'penempatan',
  `a_company_nama` varchar(255) NOT NULL DEFAULT '-',
  `a_company_kode` varchar(32) NOT NULL DEFAULT '-',
  `a_jabatan_id` int(11) DEFAULT NULL,
  `a_jabatan_nama` varchar(255) NOT NULL DEFAULT 'Staff',
  `username` varchar(24) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `welcome_message` varchar(255) NOT NULL,
  `scope` enum('all','current_below','current_only','none') NOT NULL DEFAULT 'none',
  `nip` varchar(32) DEFAULT '-',
  `alamat` varchar(255) NOT NULL,
  `alamat_kecamatan` varchar(255) NOT NULL,
  `alamat_kabkota` varchar(255) NOT NULL,
  `alamat_provinsi` varchar(255) NOT NULL,
  `alamat_negara` varchar(255) NOT NULL,
  `alamat_kodepos` varchar(12) NOT NULL,
  `tempat_lahir` varchar(255) NOT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `jenis_kelamin` int(1) NOT NULL DEFAULT '1',
  `status_pernikahan` enum('belum menikah','menikah','duda','janda') NOT NULL DEFAULT 'belum menikah',
  `telp_rumah` varchar(25) NOT NULL,
  `telp_hp` varchar(25) NOT NULL,
  `bank_rekening_nomor` varchar(255) NOT NULL,
  `bank_rekening_nama` varchar(255) NOT NULL,
  `bank_nama` varchar(255) NOT NULL,
  `kerja_terakhir` varchar(255) NOT NULL,
  `kerja_terakhir_jabatan` varchar(255) NOT NULL,
  `kerja_terakhir_gaji` float NOT NULL,
  `pendidikan_terakhir` varchar(255) NOT NULL,
  `pendidikan_terakhir_jenjang` enum('SD','SMP','SMA','S1','D3','D2','S2') NOT NULL DEFAULT 'SMA',
  `pendidikan_terakhir_tahun` year(4) NOT NULL DEFAULT '1971',
  `ibu_nama` varchar(255) NOT NULL,
  `ibu_pekerjaan` varchar(255) NOT NULL,
  `tgl_kerja_mulai` date NOT NULL,
  `tgl_kerja_akhir` date NOT NULL,
  `tgl_kontrak_akhir` date DEFAULT NULL,
  `karyawan_status` enum('Kontrak','Magang','Tetap','Harian Lepas') NOT NULL,
  `urutan` int(2) NOT NULL DEFAULT '0',
  `is_karyawan` int(1) NOT NULL DEFAULT '0',
  `is_visible` int(1) NOT NULL DEFAULT '0',
  `is_active` int(1) NOT NULL DEFAULT '1',
  `a_pengguna_id` int(11) DEFAULT NULL COMMENT 'atasan langsung'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='tabel pengguna';

-- --------------------------------------------------------

--
-- Struktur dari tabel `a_pengguna_module`
--

CREATE TABLE `a_pengguna_module` (
  `id` int(11) NOT NULL,
  `a_pengguna_id` int(11) NOT NULL,
  `a_modules_identifier` varchar(255) DEFAULT NULL,
  `rule` enum('allowed','disallowed','allowed_except','disallowed_except') NOT NULL,
  `tmp_active` enum('N','Y') NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='hak akses pengguna';

-- --------------------------------------------------------

--
-- Struktur dari tabel `a_sekolah`
--

CREATE TABLE `a_sekolah` (
  `id` int(6) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tingkat` enum('dasar','menengah','atas') NOT NULL,
  `jenis` enum('umum','kejuruan') NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `kabkota` varchar(100) NOT NULL,
  `provinsi` varchar(100) NOT NULL,
  `negara` varchar(64) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `a_sekolah`
--

INSERT INTO `a_sekolah` (`id`, `nama`, `tingkat`, `jenis`, `alamat`, `kabkota`, `provinsi`, `negara`, `is_active`) VALUES
(1, 'SMAN 1 MARGAASIH', 'atas', 'umum', '', '', '', '', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `b_guru`
--

CREATE TABLE `b_guru` (
  `id` int(8) NOT NULL,
  `a_sekolah_id` int(6) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `telp` varchar(25) NOT NULL,
  `matapelajaran` varchar(78) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `b_guru`
--

INSERT INTO `b_guru` (`id`, `a_sekolah_id`, `email`, `nama`, `password`, `telp`, `matapelajaran`, `is_active`) VALUES
(1, 1, 'ruslan@somein.co.id', 'Ruslan', '42f749ade7f9e195bf475f37a44cafcb', '08181818811', 'Bahasa Indonesia', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `c_kelas`
--

CREATE TABLE `c_kelas` (
  `id` int(10) NOT NULL,
  `a_sekolah_id` int(8) DEFAULT NULL,
  `b_guru_id` int(8) DEFAULT NULL,
  `nama` varchar(48) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `c_kelas`
--

INSERT INTO `c_kelas` (`id`, `a_sekolah_id`, `b_guru_id`, `nama`, `is_active`) VALUES
(1, 1, 1, 'X1', 1),
(2, 1, 1, 'X2', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `d_matapelajaran`
--

CREATE TABLE `d_matapelajaran` (
  `id` int(12) NOT NULL,
  `a_sekolah_id` int(11) DEFAULT NULL,
  `b_guru_id` int(11) DEFAULT NULL,
  `c_kelas_id` int(10) DEFAULT NULL,
  `nama` varchar(78) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `d_matapelajaran`
--

INSERT INTO `d_matapelajaran` (`id`, `a_sekolah_id`, `b_guru_id`, `c_kelas_id`, `nama`, `is_active`) VALUES
(1, 1, 1, 1, 'Bahasa Indonesia', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `e_siswa`
--

CREATE TABLE `e_siswa` (
  `id` int(11) NOT NULL,
  `a_sekolah_id` int(11) DEFAULT NULL,
  `b_guru_id` int(11) DEFAULT NULL,
  `c_kelas_id` int(11) DEFAULT NULL,
  `nama` varchar(255) NOT NULL,
  `nisn` varchar(48) NOT NULL,
  `nourut` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `e_soal`
--

CREATE TABLE `e_soal` (
  `id` int(14) NOT NULL,
  `b_guru_id` int(8) DEFAULT NULL,
  `c_kelas` int(10) DEFAULT NULL,
  `d_matapelajaran_id` int(12) DEFAULT NULL,
  `pertanyaan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `e_soal`
--

INSERT INTO `e_soal` (`id`, `b_guru_id`, `c_kelas`, `d_matapelajaran_id`, `pertanyaan`) VALUES
(1, 1, 1, 1, 'Siapakah Nama Presiden RI periode 2014-2019?');

-- --------------------------------------------------------

--
-- Struktur dari tabel `e_ujian`
--

CREATE TABLE `e_ujian` (
  `id` int(10) NOT NULL,
  `a_sekolah_id` int(6) DEFAULT NULL,
  `b_guru_id` int(8) DEFAULT NULL,
  `c_kelas_id` int(10) DEFAULT NULL,
  `d_matapelajaran_id` int(12) DEFAULT NULL,
  `nama` varchar(48) NOT NULL,
  `cdate` datetime NOT NULL,
  `sdate` datetime NOT NULL,
  `edate` datetime NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `e_ujian`
--

INSERT INTO `e_ujian` (`id`, `a_sekolah_id`, `b_guru_id`, `c_kelas_id`, `d_matapelajaran_id`, `nama`, `cdate`, `sdate`, `edate`, `is_active`) VALUES
(1, 1, 1, 1, 1, 'Ujian Minggu Ke-2', '2019-05-20 20:56:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `f_pilihan`
--

CREATE TABLE `f_pilihan` (
  `id` int(16) NOT NULL,
  `e_soal_id` int(14) NOT NULL,
  `jawaban` text NOT NULL,
  `is_true` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `f_pilihan`
--

INSERT INTO `f_pilihan` (`id`, `e_soal_id`, `jawaban`, `is_true`) VALUES
(1, 1, 'Ir Joko Widodo', 1),
(2, 1, 'Megawati Soekarnoputri', 0),
(3, 1, 'Susilo Bambang Yudhoyono', 0),
(4, 1, 'Moh. Hatta', 0),
(5, 1, 'Ir. Soekarno', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `g_ujian_soal`
--

CREATE TABLE `g_ujian_soal` (
  `id` int(20) NOT NULL,
  `e_ujian_id` int(14) NOT NULL,
  `e_soal_id` int(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `h_jawaban_siswa`
--

CREATE TABLE `h_jawaban_siswa` (
  `id` int(11) NOT NULL,
  `e_ujian_id` int(11) DEFAULT NULL,
  `e_soal_id` int(11) DEFAULT NULL,
  `f_pilihan_id` int(11) DEFAULT NULL,
  `cdate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `is_benar` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `i_ujianrekap`
--

CREATE TABLE `i_ujianrekap` (
  `id` int(11) NOT NULL,
  `b_guru_id` int(11) DEFAULT NULL,
  `c_kelas_id` int(11) DEFAULT NULL,
  `d_matapelajaran_id` int(11) DEFAULT NULL,
  `e_ujian_id` int(11) DEFAULT NULL,
  `nilai` decimal(4,2) NOT NULL,
  `cdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `a_media`
--
ALTER TABLE `a_media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `b_user_id` (`b_user_id`);

--
-- Indexes for table `a_modules`
--
ALTER TABLE `a_modules`
  ADD PRIMARY KEY (`identifier`),
  ADD KEY `children_identifier` (`children_identifier`);

--
-- Indexes for table `a_pengguna`
--
ALTER TABLE `a_pengguna`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `a_pengguna_username_unq` (`username`),
  ADD KEY `a_company_id` (`a_company_id`),
  ADD KEY `a_jabatan_id` (`a_jabatan_id`),
  ADD KEY `nip` (`nip`),
  ADD KEY `a_pengguna_id` (`a_pengguna_id`);

--
-- Indexes for table `a_pengguna_module`
--
ALTER TABLE `a_pengguna_module`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fka_pengguna_id` (`a_pengguna_id`),
  ADD KEY `fka_modules_identifier` (`a_modules_identifier`);

--
-- Indexes for table `a_sekolah`
--
ALTER TABLE `a_sekolah`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nama` (`nama`);

--
-- Indexes for table `b_guru`
--
ALTER TABLE `b_guru`
  ADD PRIMARY KEY (`id`),
  ADD KEY `a_sekolah_id` (`a_sekolah_id`),
  ADD KEY `a_sekolah_id_2` (`a_sekolah_id`),
  ADD KEY `email` (`email`),
  ADD KEY `matapelajaran` (`matapelajaran`);

--
-- Indexes for table `c_kelas`
--
ALTER TABLE `c_kelas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `a_sekolah_id` (`a_sekolah_id`),
  ADD KEY `b_guru_id` (`b_guru_id`);

--
-- Indexes for table `d_matapelajaran`
--
ALTER TABLE `d_matapelajaran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `a_sekolah_id` (`a_sekolah_id`),
  ADD KEY `b_guru_id` (`b_guru_id`),
  ADD KEY `c_kelas_id` (`c_kelas_id`);

--
-- Indexes for table `e_siswa`
--
ALTER TABLE `e_siswa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `a_sekolah_id` (`a_sekolah_id`),
  ADD KEY `b_guru_id` (`b_guru_id`),
  ADD KEY `c_kelas_id` (`c_kelas_id`);

--
-- Indexes for table `e_soal`
--
ALTER TABLE `e_soal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `b_guru_id` (`b_guru_id`),
  ADD KEY `c_kelas` (`c_kelas`),
  ADD KEY `d_matapelajaran_id` (`d_matapelajaran_id`);

--
-- Indexes for table `e_ujian`
--
ALTER TABLE `e_ujian`
  ADD PRIMARY KEY (`id`),
  ADD KEY `a_sekolah_id` (`a_sekolah_id`),
  ADD KEY `b_guru_id` (`b_guru_id`),
  ADD KEY `c_kelas_id` (`c_kelas_id`),
  ADD KEY `d_matapelajaran_id` (`d_matapelajaran_id`),
  ADD KEY `nama` (`nama`);

--
-- Indexes for table `f_pilihan`
--
ALTER TABLE `f_pilihan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `e_soal` (`e_soal_id`);

--
-- Indexes for table `g_ujian_soal`
--
ALTER TABLE `g_ujian_soal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `e_soal_id` (`e_soal_id`),
  ADD KEY `e_ujian_id` (`e_ujian_id`);

--
-- Indexes for table `h_jawaban_siswa`
--
ALTER TABLE `h_jawaban_siswa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `e_ujian_id` (`e_ujian_id`),
  ADD KEY `e_soal_id` (`e_soal_id`),
  ADD KEY `f_pilihan_id` (`f_pilihan_id`);

--
-- Indexes for table `i_ujianrekap`
--
ALTER TABLE `i_ujianrekap`
  ADD PRIMARY KEY (`id`),
  ADD KEY `b_guru_id` (`b_guru_id`),
  ADD KEY `c_kelas_id` (`c_kelas_id`),
  ADD KEY `d_matapelajaran_id` (`d_matapelajaran_id`),
  ADD KEY `e_ujian_id` (`e_ujian_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `a_media`
--
ALTER TABLE `a_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `a_pengguna`
--
ALTER TABLE `a_pengguna`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `a_pengguna_module`
--
ALTER TABLE `a_pengguna_module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `a_sekolah`
--
ALTER TABLE `a_sekolah`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `b_guru`
--
ALTER TABLE `b_guru`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `c_kelas`
--
ALTER TABLE `c_kelas`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `d_matapelajaran`
--
ALTER TABLE `d_matapelajaran`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `e_siswa`
--
ALTER TABLE `e_siswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `e_soal`
--
ALTER TABLE `e_soal`
  MODIFY `id` int(14) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `e_ujian`
--
ALTER TABLE `e_ujian`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `f_pilihan`
--
ALTER TABLE `f_pilihan`
  MODIFY `id` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `g_ujian_soal`
--
ALTER TABLE `g_ujian_soal`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `h_jawaban_siswa`
--
ALTER TABLE `h_jawaban_siswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `i_ujianrekap`
--
ALTER TABLE `i_ujianrekap`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `b_guru`
--
ALTER TABLE `b_guru`
  ADD CONSTRAINT `b_guru_ibfk_1` FOREIGN KEY (`a_sekolah_id`) REFERENCES `a_sekolah` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `c_kelas`
--
ALTER TABLE `c_kelas`
  ADD CONSTRAINT `c_kelas_ibfk_1` FOREIGN KEY (`a_sekolah_id`) REFERENCES `a_sekolah` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `c_kelas_ibfk_2` FOREIGN KEY (`b_guru_id`) REFERENCES `b_guru` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `d_matapelajaran`
--
ALTER TABLE `d_matapelajaran`
  ADD CONSTRAINT `d_matapelajaran_ibfk_1` FOREIGN KEY (`a_sekolah_id`) REFERENCES `a_sekolah` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `d_matapelajaran_ibfk_2` FOREIGN KEY (`b_guru_id`) REFERENCES `b_guru` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `d_matapelajaran_ibfk_3` FOREIGN KEY (`c_kelas_id`) REFERENCES `c_kelas` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `e_siswa`
--
ALTER TABLE `e_siswa`
  ADD CONSTRAINT `e_siswa_ibfk_1` FOREIGN KEY (`a_sekolah_id`) REFERENCES `a_sekolah` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `e_siswa_ibfk_2` FOREIGN KEY (`b_guru_id`) REFERENCES `b_guru` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `e_siswa_ibfk_3` FOREIGN KEY (`c_kelas_id`) REFERENCES `c_kelas` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `e_soal`
--
ALTER TABLE `e_soal`
  ADD CONSTRAINT `e_soal_ibfk_1` FOREIGN KEY (`b_guru_id`) REFERENCES `b_guru` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `e_soal_ibfk_2` FOREIGN KEY (`c_kelas`) REFERENCES `c_kelas` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `e_soal_ibfk_3` FOREIGN KEY (`d_matapelajaran_id`) REFERENCES `d_matapelajaran` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `e_ujian`
--
ALTER TABLE `e_ujian`
  ADD CONSTRAINT `e_ujian_ibfk_1` FOREIGN KEY (`a_sekolah_id`) REFERENCES `a_sekolah` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `e_ujian_ibfk_2` FOREIGN KEY (`b_guru_id`) REFERENCES `b_guru` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `e_ujian_ibfk_3` FOREIGN KEY (`c_kelas_id`) REFERENCES `c_kelas` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `e_ujian_ibfk_4` FOREIGN KEY (`d_matapelajaran_id`) REFERENCES `d_matapelajaran` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `f_pilihan`
--
ALTER TABLE `f_pilihan`
  ADD CONSTRAINT `f_pilihan_ibfk_1` FOREIGN KEY (`e_soal_id`) REFERENCES `e_soal` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `g_ujian_soal`
--
ALTER TABLE `g_ujian_soal`
  ADD CONSTRAINT `g_ujian_soal_ibfk_1` FOREIGN KEY (`e_soal_id`) REFERENCES `e_soal` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `g_ujian_soal_ibfk_2` FOREIGN KEY (`e_ujian_id`) REFERENCES `e_ujian` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `h_jawaban_siswa`
--
ALTER TABLE `h_jawaban_siswa`
  ADD CONSTRAINT `h_jawaban_siswa_ibfk_1` FOREIGN KEY (`e_soal_id`) REFERENCES `e_soal` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `h_jawaban_siswa_ibfk_2` FOREIGN KEY (`e_ujian_id`) REFERENCES `e_ujian` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `h_jawaban_siswa_ibfk_3` FOREIGN KEY (`f_pilihan_id`) REFERENCES `f_pilihan` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `i_ujianrekap`
--
ALTER TABLE `i_ujianrekap`
  ADD CONSTRAINT `i_ujianrekap_ibfk_1` FOREIGN KEY (`b_guru_id`) REFERENCES `b_guru` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `i_ujianrekap_ibfk_2` FOREIGN KEY (`c_kelas_id`) REFERENCES `c_kelas` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `i_ujianrekap_ibfk_3` FOREIGN KEY (`d_matapelajaran_id`) REFERENCES `d_matapelajaran` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `i_ujianrekap_ibfk_4` FOREIGN KEY (`e_ujian_id`) REFERENCES `e_ujian` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
