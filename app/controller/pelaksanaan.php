<?php
class Pelaksanaan extends JI_Controller {
  public function __construct(){
    parent::__construct();
    $this->setTheme("front/");
    $this->load("front/a_sekolah_model",'asm');
    $this->load("front/b_guru_model",'bgm');
    $this->load("front/c_kelas_model",'ckm');
    $this->load("front/c_matapelajaran_model",'cmpm');
    $this->load("front/d_ujian_model",'dum');
    $this->load("front/f_pelaksanaan_model",'fpm');
    $this->page_current = 'pelaksanaan';
  }
  protected function __sifat($sifat){
    switch($sifat){
      case "open" :
        $sifat = 'Open Book';
        break;
      case "close" :
        $sifat = 'Close Book';
        break;
      default: $sifat='hide';
    }
    return $sifat;
  }
  public function index(){
    $data = $this->__init();
    //$this->debug($data);
    if(!$this->user_login){
      redir(base_url("login"),0,0);
      die();
    }
    $pengguna = $data['sess']->user;

    $data['sekolah'] = $this->asm->getById($pengguna->a_sekolah_id);
    $data['kelas'] = $this->ckm->getBySekolahId($pengguna->a_sekolah_id);
    $data['matapelajaran'] = $this->cmpm->getMataPelajaranSaya($pengguna->a_sekolah_id,$pengguna->id);

    //load datatables
    $this->loadCss($this->cdn_url("assets/css/dataTables.bootstrap.min"),"before");
    $this->putJsFooter($this->cdn_url("assets/js/jquery.dataTables.min"));
    $this->putJsFooter($this->cdn_url("assets/js/dataTables.bootstrap.min"));

    //load datepicker
    $this->loadCss($this->cdn_url("assets/css/bootstrap-datepicker.min"));
    $this->putJsFooter($this->cdn_url("assets/js/bootstrap-datepicker.min"));

    //load clockpicker
    $this->loadCss($this->cdn_url("assets/css/bootstrap-clockpicker.min"));
    $this->putJsFooter($this->cdn_url("assets/js/bootstrap-clockpicker.min"));

    $this->setTitle("Pelaksanaan Ujian ".$this->site_suffix);
    $this->__breadCrumb("Pelaksanaan Ujian");
    $this->putThemeContent("pelaksanaan/home_modal",$data);
    $this->putThemeContent("pelaksanaan/home",$data);
		$this->putJsContent("pelaksanaan/home_bottom",$data);
    $this->loadLayout("col-1-dashboard",$data);
    $this->render();
  }

  public function ujian($id){
    $data = $this->__init();
    //$this->debug($data);
    if(!$this->user_login){
      redir(base_url("login"),0,0);
      die();
    }
    $pengguna = $data['sess']->user;

    //check id
    $id = (int) $id;
    if($id<=0){
      redir(base_url("pelaksanaan"),0,0);
      die();
    }

    //check ujian
  	$ujian = $this->dum->getByIdSekolahIdGuruId($pengguna->a_sekolah_id,$pengguna->id,$id);
    if(!isset($ujian->id)){
      redir(base_url("ujian"),0,0);
      die();
    }

    $data['sekolah'] = $this->asm->getById($pengguna->a_sekolah_id);
    $data['kelas'] = $this->ckm->getBySekolahId($pengguna->a_sekolah_id);
    $data['matapelajaran'] = $this->cmpm->getMataPelajaranSaya($pengguna->a_sekolah_id,$pengguna->id);
    $matpel = array();
    foreach($data['matapelajaran'] as $mp){
      $matpel[$mp->id] = $mp;
    }
    unset($mp);
    $data['matapelajaran'] = $matpel;
    unset($matpel);
    $data['ujian'] = $ujian;
    //load datatables
    $this->putJsFooter("https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js");
    $this->putJsFooter($this->cdn_url("assets/js/jquery.scrollto.min.js"));
    //$this->putJsFooter($this->cdn_url("assets/js/responsive.bootstrap.min"));

    $this->setTitle("Pelaksanaan ".$this->site_suffix);
    $this->__breadCrumb("Pelaksanaan",base_url("pelaksanaan"));
    $this->__breadCrumb("Ujian");
    $this->putThemeContent("pelaksanaan/ujian_modal",$data);
    $this->putThemeContent("pelaksanaan/ujian",$data);
		$this->putJsContent("pelaksanaan/ujian_bottom",$data);
    $this->loadLayout("col-1-dashboard",$data);
    $this->render();
  }

  public function detail($id){
    $data = $this->__init();
    //$this->debug($data);
    if(!$this->user_login){
      redir(base_url("login"),0,0);
      die();
    }
    $pengguna = $data['sess']->user;

    //check id
    $id = (int) $id;
    if($id<=0){
      redir(base_url("pelaksanaan"),0,0);
      die();
    }

    //check ujian
  	$pelaksanaan = $this->fpm->getByIdGuruId($pengguna->id,$id);
    if(!isset($pelaksanaan->id)){
      redir(base_url("pelaksanaan"),0,0);
      die();
    }

  	$ujian = $this->dum->getByIdGuruId($pengguna->id,$pelaksanaan->d_ujian_id);
    if(!isset($ujian->id)){
      redir(base_url("pelaksanaan"),0,0);
      die();
    }

    $data['sekolah'] = $this->asm->getById($pengguna->a_sekolah_id);
    $data['kelas'] = $this->ckm->getBySekolahId($pengguna->a_sekolah_id);
    $data['matapelajaran'] = $this->cmpm->getMataPelajaranSaya($pengguna->a_sekolah_id,$pengguna->id);
    $data['pelaksanaan'] = $pelaksanaan;
    $data['ujian'] = $ujian;
    $matpel = array();
    foreach($data['matapelajaran'] as $mp){
      $matpel[$mp->id] = $mp;
    }
    unset($mp);
    $data['matapelajaran'] = $matpel;
    unset($matpel);

    $data['pelaksanaan_kelas'] = $this->ckm->getById($pelaksanaan->c_kelas_id);

    //load datatables
    $this->putJsFooter("https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js");
    $this->putJsFooter($this->cdn_url("assets/js/jquery.scrollto.min.js"));
    //$this->putJsFooter($this->cdn_url("assets/js/responsive.bootstrap.min"));

    $this->setTitle("Detail Pelaksanaan Ujian ".$this->site_suffix);
    $this->__breadCrumb("Pelaksanaan Ujian",base_url("pelaksanaan"));
    $this->__breadCrumb("Detail");
    $this->putThemeContent("pelaksanaan/detail_modal",$data);
    $this->putThemeContent("pelaksanaan/detail",$data);
		$this->putJsContent("pelaksanaan/detail_bottom",$data);
    $this->loadLayout("col-1-dashboard",$data);
    $this->render();
  }
}
