<?php
class Sekolah extends JI_Controller {
  public function __construct(){
    parent::__construct();
    $this->setTheme("front/");
    $this->load("front/a_sekolah_model",'asm');
    $this->load("front/b_guru_model",'bgm');
    $this->page_current = 'kelas';
  }
  public function index(){
    $data = $this->__init();
    //$this->debug($data);
    if(!$this->user_login){
      redir(base_url("login"),0,0);
      die();
    }

    $alamat = $this->input->post("alamat");
    $kabkota = $this->input->post("kabkota");
    $provinsi = $this->input->post("provinsi");
    if(empty($alamat)) $alamat = '';
    if(empty($kabkota)) $kabkota = '';
    if(empty($provinsi)) $provinsi = '';
    if(strlen($alamat)>0){
      $a_sekolah_id = $data['sess']->user->a_sekolah_id;
      $du = array();
      $du['alamat'] = $alamat;
      $du['kabkota'] = $kabkota;
      $du['provinsi'] = $provinsi;
      $res = $this->asm->update($a_sekolah_id,$du);
      if($res){
        $data['notif'] = 'Data sekolah berhasil diubah';
      }
    }

    $data['sekolah'] = $this->asm->getById($data['sess']->user->a_sekolah_id);
    $this->setTitle("Sekolah ".$this->site_suffix);
    $this->__breadCrumb("Sekolah");
    $this->putThemeContent("sekolah/home",$data);
    $this->loadLayout("col-1-dashboard",$data);
    $this->render();
  }
}
