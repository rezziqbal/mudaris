<?php
class Dashboard extends JI_Controller {
  public function __construct(){
    parent::__construct();
    $this->setTheme("front/");
  }
  public function index(){
    $data = $this->__init();
    //$this->debug($data);
    if(!$this->user_login){
      redir(base_url("login"),0,0);
      die();
    }
    $this->setTitle("Dashboard ".$this->site_suffix);

    $this->putThemeContent("dashboard/home",$data);
    $this->loadLayout("col-1-dashboard",$data);
    $this->render();
  }
}
