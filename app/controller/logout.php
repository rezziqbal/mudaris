<?php
class Logout extends JI_Controller {
  public function __construct(){
    parent::__construct();
    $this->setTheme("front/");
    $this->lib("seme_purifier");
    $this->load("front/a_sekolah_model","asm");
    $this->load("front/b_guru_model","bgm");
  }
  public function index(){
    $sess = $this->getKey();
    if(!isset($sess->user)) $sess = new stdClass();
    $sess->user = new stdClass();
    $this->setKey($sess);
    redir(base_url(""),0,0);
  }
}
