<?php
class Siswa extends JI_Controller {
  public function __construct(){
    parent::__construct();
    $this->setTheme("front/");
    $this->load("front/a_sekolah_model",'asm');
    $this->load("front/b_guru_model",'bgm');
    $this->load("front/c_kelas_model",'ckm');
    $this->page_current = 'siswa';
  }
  public function index(){
    $data = $this->__init();
    //$this->debug($data);
    if(!$this->user_login){
      redir(base_url("login"),0,0);
      die();
    }
    
    $data['sekolah'] = $this->asm->getById($data['sess']->user->a_sekolah_id);
    $data['kelas'] = $this->ckm->getBySekolahId($data['sess']->user->a_sekolah_id);

    //load datatables
    $this->loadCss($this->cdn_url("assets/css/dataTables.bootstrap.min"),"before");
    $this->putJsFooter($this->cdn_url("assets/js/jquery.dataTables.min"));
    $this->putJsFooter($this->cdn_url("assets/js/dataTables.bootstrap.min"));
    //$this->putJsFooter($this->cdn_url("assets/js/responsive.bootstrap.min"));

    $this->setTitle("Siswa ".$this->site_suffix);
    $this->__breadCrumb("Siswa");
    $this->putThemeContent("siswa/home_modal",$data);
    $this->putThemeContent("siswa/home",$data);
		$this->putJsContent("siswa/home_bottom",$data);
    $this->loadLayout("col-1-dashboard",$data);
    $this->render();
  }
}
