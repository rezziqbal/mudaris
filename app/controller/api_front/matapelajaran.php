<?php
class MataPelajaran extends JI_Controller {
  public function __construct(){
    parent::__construct();
    $this->setTheme("front/");
    $this->load("front/a_sekolah_model",'asm');
    $this->load("front/b_guru_model",'bgm');
    $this->load("api_front/c_kelas_model",'ckm');
    $this->load("api_front/c_matapelajaran_model",'cmpm');
    $this->page_current = 'kelas';
  }
  private function __sanitizePost($excepts=array()){
    foreach($_POST as $key=>&$val){
      if(is_string($val)){
        if(in_array($key,$excepts)){
          $val = $this->seme_purifier->richtext($val);
        }else{
          $val = $this->__f($val);
        }
      }
    }
  }
  public function index(){
    $d = $this->__init();
    $data = array();
    if(!$this->user_login){
      $this->status = 400;
      $this->message = 'Harus login';
      header("HTTP/1.0 400 Harus login");
      $this->__json_out($data);
      die();
    }
    $pengguna = $d['sess']->user;
    $a_sekolah_id = $pengguna->a_sekolah_id;

    $draw = $this->input->post("draw");
    $sval = $this->input->post("search");
    $sSearch = $this->input->request("sSearch");
    $sEcho = $this->input->post("sEcho");
    $page = $this->input->post("iDisplayStart");
    $pagesize = $this->input->request("iDisplayLength");

    $iSortCol_0 = $this->input->post("iSortCol_0");
    $sSortDir_0 = $this->input->post("sSortDir_0");


    $sortCol = "date";
    $sortDir = strtoupper($sSortDir_0);
    if(empty($sortDir)) $sortDir = "DESC";
    if(strtolower($sortDir) != "desc"){
      $sortDir = "ASC";
    }

    switch($iSortCol_0){
      case 0:
        $sortCol = "id";
        break;
      case 1:
        $sortCol = "nama";
        break;
      case 2:
        $sortCol = "is_active";
        break;
      default:
        $sortCol = "id";
    }

    if(empty($draw)) $draw = 0;
    if(empty($pagesize)) $pagesize=10;
    if(empty($page)) $page=0;
    $keyword = $sSearch;

    $this->status = '100';
    $this->message = 'Berhasil';
    $dcount = $this->cmpm->countBySekolahId($a_sekolah_id,$keyword);
    $ddata = $this->cmpm->getBySekolahId($a_sekolah_id,$page,$pagesize,$sortCol,$sortDir,$keyword);

    foreach($ddata as &$gd){
      if(isset($gd->is_active)){
        if(!empty($gd->is_active)){
          $gd->is_active = '<span class="label label-success">aktif</span>';
        }else{
          $gd->is_active = '<span class="label label-default">tidak aktif</span>';
        }
      }
    }
    //sleep(3);
    $another = array();
    $this->__jsonDataTable($ddata,$dcount);
  }
  public function detail($id){
    $d = $this->__init();
    $data = new stdClass();
    if(!$this->user_login){
      $this->status = 400;
      $this->message = 'Harus login';
      header("HTTP/1.0 400 Harus login");
      $this->__json_out($data);
      die();
    }
    $pengguna = $d['sess']->user;
    $a_sekolah_id = $pengguna->a_sekolah_id;

    $id = (int) $id;
    if($id<=0){
      $this->status = 1001;
      $this->message = 'ID Kelas tidak valid atau tidak termasuk kedalam sekolah ini';
      $this->__json_out($data);
      die();
    }
    $data = $this->cmpm->getByIdDanSekolahId($id,$a_sekolah_id);

    $this->status = 200;
    $this->message = 'Berhasil';
    $this->__json_out($data);
  }
  public function tambah(){
    $d = $this->__init();
    $data = new stdClass();
    if(!$this->user_login){
      $this->status = 400;
      $this->message = 'Harus login';
      header("HTTP/1.0 400 Harus login");
      $this->__json_out($data);
      die();
    }
    $pengguna = $d['sess']->user;
    $a_sekolah_id = $pengguna->a_sekolah_id;

    //sanitize post
    $this->__sanitizePost();

    //collection input
    $nama = $this->input->post("nama");
    $is_active = 1;

    //input validation
    if(empty($nama)){
      $this->status = 1005;
      $this->message = 'Nama kelas minimal 1 huruf';
      $this->__json_out($data);
      die();
    }

    $di = array();
    $di['a_sekolah_id'] = $a_sekolah_id;
    $di['b_guru_id'] = $pengguna->id;
    $di['nama'] = $nama;
    $di['is_active'] = $is_active;
    $res = $this->cmpm->set($di);
    if($res){
      $this->status = 200;
      $this->message = 'Berhasil';
    }else{
      $this->status = 1003;
      $this->message = 'Failed inserting to database';
    }
    $this->__json_out($data);
  }
  public function edit($id){
    $d = $this->__init();
    $data = new stdClass();
    if(!$this->user_login){
      $this->status = 400;
      $this->message = 'Harus login';
      header("HTTP/1.0 400 Harus login");
      $this->__json_out($data);
      die();
    }
    $pengguna = $d['sess']->user;
    $a_sekolah_id = $pengguna->a_sekolah_id;

    $id = (int) $id;
    if($id<=0){
      $this->status = 1001;
      $this->message = 'ID Kelas tidak valid atau tidak termasuk kedalam sekolah ini';
      $this->__json_out($data);
      die();
    }

    //sanitize post
    $this->__sanitizePost();

    //collection input
    $nama = $this->input->post("nama");
    $is_active = (int) $this->input->post("is_active");

    //input validation
    if(empty($nama)){
      $this->status = 1002;
      $this->message = 'Nama kelas minimal 1 huruf';
      $this->__json_out($data);
      die();
    }

    $kelas = $this->cmpm->getByIdDanSekolahId($id,$a_sekolah_id);
    if(!isset($kelas->id)){
      $this->status = 1003;
      $this->message = 'Kelas tidak ditemukan atau mungkin sudah dihapus';
      $this->__json_out($data);
      die();
    }

    $du = array();
    $du['nama'] = $nama;
    $du['is_active'] = $is_active;
    $res = $this->cmpm->updateByIdSekolahId($a_sekolah_id,$id,$du);
    if($res){
      $this->status = 200;
      $this->message = 'Berhasil';
    }else{
      $this->status = 1003;
      $this->message = 'Failed updating to database';
    }
    $this->__json_out($data);
  }
  public function hapus($id){
    $d = $this->__init();
    $data = new stdClass();
    if(!$this->user_login){
      $this->status = 400;
      $this->message = 'Harus login';
      header("HTTP/1.0 400 Harus login");
      $this->__json_out($data);
      die();
    }
    $pengguna = $d['sess']->user;
    $a_sekolah_id = $pengguna->a_sekolah_id;

    $id = (int) $id;
    if($id<=0){
      $this->status = 1001;
      $this->message = 'ID Kelas tidak valid atau tidak termasuk kedalam sekolah ini';
      $this->__json_out($data);
      die();
    }

    //sanitize post
    $this->__sanitizePost();

    //collection input
    $nama = $this->input->post("nama");
    $is_active = (int) $this->input->post("is_active");

    //input validation
    if(empty($nama)){
      $this->status = 1002;
      $this->message = 'Nama kelas minimal 1 huruf';
      $this->__json_out($data);
      die();
    }

    $kelas = $this->cmpm->getByIdDanSekolahId($id,$a_sekolah_id);
    if(!isset($kelas->id)){
      $this->status = 1003;
      $this->message = 'Kelas tidak ditemukan atau mungkin sudah dihapus';
      $this->__json_out($data);
      die();
    }

    //lakukan proses penghapusan
    $res = $this->cmpm->delByIdSekolahId($a_sekolah_id,$id);
    if($res){
      $this->status = 200;
      $this->message = 'Berhasil';
    }else{
      $this->status = 1003;
      $this->message = 'Failed updating to database';
    }
    $this->__json_out($data);
  }
}
