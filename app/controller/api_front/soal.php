<?php
class Soal extends JI_Controller {
  public function __construct(){
    parent::__construct();
    $this->setTheme("front/");
    $this->lib("seme_purifier");
    $this->load("front/a_sekolah_model",'asm');
    $this->load("front/b_guru_model",'bgm');
    $this->load("api_front/d_ujian_model",'dum');
    $this->load("api_front/e_soal_model",'esm');
    $this->load("api_front/e_soal_pilihan_model",'espm');
    $this->page_current = 'kelas';
  }
  private function __sanitizePost($excepts=array()){
		foreach($_POST as $key=>&$val){
			if(is_string($val)){
				if(in_array($key,$excepts)){
					$val = $this->seme_purifier->richtext($val);
				}else{
					$val = $this->__f($val);
				}
			}
		}
  }
  public function index($d_ujian_id=""){
		$d = $this->__init();
		$data = array();
		if(!$this->user_login){
			$this->status = 400;
			$this->message = 'Harus login';
			header("HTTP/1.0 400 Harus login");
			$this->__json_out($data);
			die();
		}
    $pengguna = $d['sess']->user;
    $a_sekolah_id = $pengguna->a_sekolah_id;

    $d_ujian_id = (int) $d_ujian_id;
    if($d_ujian_id<=0){
			$this->status = 900;
			$this->message = 'Ujian ID tidak valid';
			$this->__json_out($data);
			die();
    }

    $ujian = $this->dum->getByIdSekolahIdGuruId($a_sekolah_id,$pengguna->id,$d_ujian_id);
    if(!isset($ujian->id)){
			$this->status = 499;
			$this->message = 'Ujian tidak ditemukan atau telah dihapus';
			$this->__json_out($data);
      die();
    }
    $this->status = 200;
    $this->message = 'Berhasil';

    $pilihans = array();
    $soal = $this->esm->getByGuruIdUjianId($pengguna->id,$d_ujian_id);
    $options = $this->espm->getByGuruIdUjianId($pengguna->id,$d_ujian_id);
    foreach($options as $opt){
      $id = (int) $opt->e_soal_id;
      if(!isset($pilihans[$id])) $pilihans[$id] = array();
      $pilihans[$id][] = $opt;
    }
    unset($opt);
    unset($options);
    foreach($soal as &$sl){
      $id = (int) $sl->id;
      $sl->pilihan = array();
      if(isset($pilihans[$id])) $sl->pilihan = $pilihans[$id];
      unset($sl->b_guru_id);
      unset($sl->d_ujian_id);
    }
    $data = $soal;
    $this->__json_out($data);
  }
  public function detail($e_ujian_id,$id){
		$d = $this->__init();
		$data = new stdClass();
		if(!$this->user_login){
			$this->status = 400;
			$this->message = 'Harus login';
			header("HTTP/1.0 400 Harus login");
			$this->__json_out($data);
			die();
		}
    $pengguna = $d['sess']->user;
    $a_sekolah_id = $pengguna->a_sekolah_id;

    $e_ujian_id = (int) $e_ujian_id;
    if($e_ujian_id<=0){
			$this->status = 900;
			$this->message = 'Ujian ID tidak valid';
			$this->__json_out($data);
			die();
    }

    $ujian = $this->esm->getByIdSekolahIdGuruId($a_sekolah_id,$pengguna->id,$e_ujian_id);
    if(!isset($ujian->id)){
			$this->status = 499;
			$this->message = 'Ujian tidak ditemukan atau telah dihapus';
      die();
    }


    $this->status = 200;
    $this->message = 'Berhasil';
    $this->__json_out($data);
  }
  public function tambah(){
		$d = $this->__init();
		$data = new stdClass();
		if(!$this->user_login){
			$this->status = 400;
			$this->message = 'Harus login';
			header("HTTP/1.0 400 Harus login");
			$this->__json_out($data);
			die();
		}
    $pengguna = $d['sess']->user;
    $a_sekolah_id = $pengguna->a_sekolah_id;

    //sanitize post
    //$this->__sanitizePost(array("soal"));

    //collection input
    $d_ujian_id = (int) $this->input->post("d_ujian_id");
    $soal = $this->input->post("soal");
    if(empty($soal)) $soal = '';
    $is_active = 1;

    //input validation
    if($d_ujian_id<=0){
			$this->status = 1010;
			$this->message = 'Ujian ID tidak valid';
			$this->__json_out($data);
			die();
    }
    $ujian = $this->dum->getByIdSekolahIdGuruId($a_sekolah_id,$pengguna->id,$d_ujian_id);
    if(!isset($ujian->id)){
			$this->status = 1011;
			$this->message = 'Ujian ID tidak dapat ditemukan atau telah dihapus';
			$this->__json_out($data);
			die();
    }
    $this->esm->trans_start();
    $di = array();
    $di['b_guru_id'] = $pengguna->id;
    $di['d_ujian_id'] = $d_ujian_id;
    $di['soal'] = $this->seme_purifier->richtext($soal);
    $res = $this->esm->set($di);
    if($res){
      $this->esm->trans_commit();
      $this->status = 200;
      $this->message = 'Berhasil';
      $e_soal_id = $res;

      //begin insert pilihan soal
      $pilihans = array();
      for($i=1;$i<=$ujian->jml_opsi;$i++){
        $pilihan = array();
        $pilihan['e_soal_id'] = $e_soal_id;
        $pilihan['pilihan'] = $this->seme_purifier->richtext($this->input->post("pilihan$i"));
        $pilihan['is_true'] = (int) $this->input->post("is_true$i");
        $pilihans[] = $pilihan;
      }
      $this->espm->setMass($pilihans);

    }else{
      $this->esm->trans_rollback();
      $this->status = 1003;
      $this->message = 'Failed inserting to database';
    }
    $this->esm->trans_end();
    $this->__json_out($data);
  }
  public function edit($id){
		$d = $this->__init();
		$data = new stdClass();
		if(!$this->user_login){
			$this->status = 400;
			$this->message = 'Harus login';
			header("HTTP/1.0 400 Harus login");
			$this->__json_out($data);
			die();
		}
    $pengguna = $d['sess']->user;
    $a_sekolah_id = $pengguna->a_sekolah_id;

    $id = (int) $id;
    if($id<=0){
			$this->status = 1001;
			$this->message = 'ID Kelas tidak valid atau tidak termasuk kedalam sekolah ini';
			$this->__json_out($data);
			die();
    }

    //sanitize post
    $this->__sanitizePost();

    //collection input
    $nama = $this->input->post("nama");
    $is_active = (int) $this->input->post("is_active");

    //input validation
    if(empty($nama)){
			$this->status = 1002;
			$this->message = 'Nama kelas minimal 1 huruf';
			$this->__json_out($data);
			die();
    }

    $kelas = $this->ckm->getByIdDanSekolahId($id,$a_sekolah_id);
    if(!isset($kelas->id)){
      $this->status = 1003;
      $this->message = 'Kelas tidak ditemukan atau mungkin sudah dihapus';
      $this->__json_out($data);
      die();
    }

    $du = array();
    $du['nama'] = $nama;
    $du['is_active'] = $is_active;
    $res = $this->ckm->updateByIdSekolahId($a_sekolah_id,$id,$du);
    if($res){
      $this->status = 200;
      $this->message = 'Berhasil';
    }else{
      $this->status = 1003;
      $this->message = 'Failed updating to database';
    }
    $this->__json_out($data);
  }
  public function hapus($id){
		$d = $this->__init();
		$data = new stdClass();
		if(!$this->user_login){
			$this->status = 400;
			$this->message = 'Harus login';
			header("HTTP/1.0 400 Harus login");
			$this->__json_out($data);
			die();
		}
    $pengguna = $d['sess']->user;
    $a_sekolah_id = $pengguna->a_sekolah_id;

    $id = (int) $id;
    if($id<=0){
			$this->status = 1001;
			$this->message = 'ID Kelas tidak valid atau tidak termasuk kedalam sekolah ini';
			$this->__json_out($data);
			die();
    }

    //sanitize post
    $this->__sanitizePost();

    //collection input
    $nama = $this->input->post("nama");
    $is_active = (int) $this->input->post("is_active");

    //input validation
    if(empty($nama)){
			$this->status = 1002;
			$this->message = 'Nama kelas minimal 1 huruf';
			$this->__json_out($data);
			die();
    }

    $kelas = $this->ckm->getByIdDanSekolahId($id,$a_sekolah_id);
    if(!isset($kelas->id)){
      $this->status = 1003;
      $this->message = 'Kelas tidak ditemukan atau mungkin sudah dihapus';
      $this->__json_out($data);
      die();
    }

    //lakukan proses penghapusan
    $res = $this->ckm->delByIdSekolahId($a_sekolah_id,$id);
    if($res){
      $this->status = 200;
      $this->message = 'Berhasil';
    }else{
      $this->status = 1003;
      $this->message = 'Failed updating to database';
    }
    $this->__json_out($data);
  }
}
