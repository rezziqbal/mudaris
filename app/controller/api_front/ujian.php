<?php
class Ujian extends JI_Controller {
  public function __construct(){
    parent::__construct();
    $this->setTheme("front/");
    $this->load("front/a_sekolah_model",'asm');
    $this->load("front/b_guru_model",'bgm');
    $this->load("api_front/c_kelas_model",'ckm');
    $this->load("api_front/d_siswa_model",'dsm');
    $this->load("api_front/d_ujian_model",'dum');
    $this->page_current = 'kelas';
  }
  private function __sanitizePost($excepts=array()){
    foreach($_POST as $key=>&$val){
      if(is_string($val)){
        if(in_array($key,$excepts)){
          $val = $this->seme_purifier->richtext($val);
        }else{
          $val = $this->__f($val);
        }
      }
    }
  }
  public function index(){
    $d = $this->__init();
    $data = array();
    if(!$this->user_login){
      $this->status = 400;
      $this->message = 'Harus login';
      header("HTTP/1.0 400 Harus login");
      $this->__json_out($data);
      die();
    }
    $pengguna = $d['sess']->user;
    $a_sekolah_id = $pengguna->a_sekolah_id;

    $tbl_as = $this->dum->getTableAlias();
    $tbl2_as = $this->dum->getTableAlias2();

    $draw = $this->input->post("draw");
    $sval = $this->input->post("search");
    $sSearch = $this->input->request("sSearch");
    $sEcho = $this->input->post("sEcho");
    $page = $this->input->post("iDisplayStart");
    $pagesize = $this->input->request("iDisplayLength");

    $iSortCol_0 = $this->input->post("iSortCol_0");
    $sSortDir_0 = $this->input->post("sSortDir_0");


    $sortCol = "date";
    $sortDir = strtoupper($sSortDir_0);
    if(empty($sortDir)) $sortDir = "DESC";
    if(strtolower($sortDir) != "desc"){
      $sortDir = "ASC";
    }

    switch($iSortCol_0){
      case 0:
        $sortCol = "$tbl_as.id";
        break;
      case 1:
        $sortCol = "COALESCE($tbl2_as.nama,'-') $sortDir, nourut";
        $sortDir = 'ASC';
        break;
      case 2:
        $sortCol = "COALESCE($tbl2_as.nama,'-') ASC, nourut";
        break;
      case 3:
        $sortCol = "$tbl_as.nama";
        break;
      case 4:
        $sortCol = "$tbl_as.is_active";
        break;
      default:
        $sortCol = "$tbl_as.id";
    }

    if(empty($draw)) $draw = 0;
    if(empty($pagesize)) $pagesize=10;
    if(empty($page)) $page=0;
    $keyword = $sSearch;

    $this->status = '100';
    $this->message = 'Berhasil';
    $dcount = $this->dum->countBySekolahIdGuruId($a_sekolah_id,$pengguna->id,$keyword);
    $ddata = $this->dum->getBySekolahIdGuruId($a_sekolah_id,$pengguna->id,$page,$pagesize,$sortCol,$sortDir,$keyword);

    foreach($ddata as &$gd){
      if(isset($gd->is_active)){
        $is_active = '';
        if(isset($gd->sifat)){
          switch($gd->sifat){
            case "open":
              $is_active .= ' <span class="label label-default">Open Book</span>';
              break;
            case "close":
              $is_active .= ' <span class="label label-default">Closed Book</span>';
              break;
          }
        }
        if(isset($gd->ujian_status)){
          switch($gd->ujian_status){
            case "sedang_berjalan":
              $is_active .= ' <span class="label label-info">Sedang berlangsung</span>';
              break;
            case "selesai":
              $is_active .= ' <span class="label label-info">Selesai</span>';
              break;
            default :
              $is_active .= ' <span class="label label-default">Belum Dimulai</span>';
          }
        }
        if(!empty($gd->is_active)){
          $is_active .= ' <span class="label label-success">aktif</span>';
        }else{
          $is_active .= ' <span class="label label-default">tidak aktif</span>';
        }

        $gd->is_active = $is_active;
      }
    }
    //sleep(3);
    $another = array();
    $this->__jsonDataTable($ddata,$dcount);
  }
  public function detail($id){
    $d = $this->__init();
    $data = new stdClass();
    if(!$this->user_login){
      $this->status = 400;
      $this->message = 'Harus login';
      header("HTTP/1.0 400 Harus login");
      $this->__json_out($data);
      die();
    }
    $pengguna = $d['sess']->user;
    $a_sekolah_id = $pengguna->a_sekolah_id;

    $id = (int) $id;
    if($id<=0){
      $this->status = 1001;
      $this->message = 'ID Kelas tidak valid atau tidak termasuk kedalam sekolah ini';
      $this->__json_out($data);
      die();
    }

    $data = $this->dum->getByIdSekolahIdGuruId($a_sekolah_id,$pengguna->id,$id);

    $this->status = 200;
    $this->message = 'Berhasil';
    $this->__json_out($data);
  }
  public function tambah(){
    $d = $this->__init();
    $data = new stdClass();
    if(!$this->user_login){
      $this->status = 400;
      $this->message = 'Harus login';
      header("HTTP/1.0 400 Harus login");
      $this->__json_out($data);
      die();
    }
    $pengguna = $d['sess']->user;
    $a_sekolah_id = $pengguna->a_sekolah_id;


    //sanitize post
    $this->__sanitizePost();

    //collection input
    $c_matapelajaran_id = (int) $this->input->post("c_matapelajaran_id");
    $nourut = (int) $this->input->post("nourut");
    $nama = $this->input->post("nama");
    $sifat = $this->input->post("sifat");
    $jml_opsi = (int) $this->input->post("jml_opsi");
    $is_active = 1;

    //input validation
    if(empty($nama)){
      $this->status = 1005;
      $this->message = 'Nama kelas minimal 1 huruf';
      $this->__json_out($data);
      die();
    }
    if($jml_opsi<=1){
      $this->status = 1006;
      $this->message = 'Jumlah pilihan minimal 2';
      $this->__json_out($data);
      die();
    }
    if($c_matapelajaran_id<=0) $c_matapelajaran_id = "null";
    if($nourut<=0) $nourut = $this->dum->getNoUrut($a_sekolah_id,$pengguna->id,$c_matapelajaran_id);

    $di = array();
    $di['a_sekolah_id'] = $a_sekolah_id;
    $di['b_guru_id'] = $pengguna->id;
    $di['c_matapelajaran_id'] = $c_matapelajaran_id;
    $di['nourut'] = $nourut;
    $di['nama'] = $nama;
    $di['sifat'] = $sifat;
    $di['jml_opsi'] = $jml_opsi;
    $di['is_active'] = $is_active;
    $res = $this->dum->set($di);
    if($res){
      $this->status = 200;
      $this->message = 'Berhasil';
    }else{
      $this->status = 1003;
      $this->message = 'Failed inserting to database';
    }
    $this->__json_out($data);
  }
  public function edit($id){
    $d = $this->__init();
    $data = new stdClass();
    if(!$this->user_login){
      $this->status = 400;
      $this->message = 'Harus login';
      header("HTTP/1.0 400 Harus login");
      $this->__json_out($data);
      die();
    }
    $pengguna = $d['sess']->user;
    $a_sekolah_id = $pengguna->a_sekolah_id;

    $id = (int) $id;
    if($id<=0){
      $this->status = 1001;
      $this->message = 'ID Kelas tidak valid atau tidak termasuk kedalam sekolah ini';
      $this->__json_out($data);
      die();
    }

    //sanitize post
    $this->__sanitizePost();

    //collection input
    $c_matapelajaran_id = (int) $this->input->post("c_matapelajaran_id");
    $nourut = (int) $this->input->post("nourut");
    $nama = $this->input->post("nama");
    $sifat = $this->input->post("sifat");
    $jml_opsi = (int) $this->input->post("jml_opsi");
    $is_active = (int) $this->input->post("is_active");

    //input validation
    if(empty($nama)){
      $this->status = 1002;
      $this->message = 'Nama kelas minimal 1 huruf';
      $this->__json_out($data);
      die();
    }
    if($jml_opsi<=1){
      $this->status = 1007;
      $this->message = 'Jumlah pilihan minimal 2';
      $this->__json_out($data);
      die();
    }
    if($c_matapelajaran_id<=0) $c_matapelajaran_id = "null";
    if($nourut<=0) $nourut = $this->dum->getNoUrut($a_sekolah_id,$pengguna->id,$c_matapelajaran_id);

    $kelas = $this->dum->getByIdSekolahIdGuruId($a_sekolah_id,$pengguna->id,$id);
    if(!isset($kelas->id)){
      $this->status = 1003;
      $this->message = 'Kelas tidak ditemukan atau mungkin sudah dihapus';
      $this->__json_out($data);
      die();
    }

    $du = array();
    $du['c_matapelajaran_id'] = $c_matapelajaran_id;
    $du['nourut'] = $nourut;
    $du['nama'] = $nama;
    $du['sifat'] = $sifat;
    $du['jml_opsi'] = $jml_opsi;
    $du['is_active'] = $is_active;
    $res = $this->dum->updateByIdSekolahIdGuruId($a_sekolah_id,$pengguna->id,$id,$du);
    if($res){
      $this->status = 200;
      $this->message = 'Berhasil';
    }else{
      $this->status = 1003;
      $this->message = 'Failed updating to database';
    }
    $this->__json_out($data);
  }
  public function hapus($id){
    $d = $this->__init();
    $data = new stdClass();
    if(!$this->user_login){
      $this->status = 400;
      $this->message = 'Harus login';
      header("HTTP/1.0 400 Harus login");
      $this->__json_out($data);
      die();
    }
    $pengguna = $d['sess']->user;
    $a_sekolah_id = $pengguna->a_sekolah_id;

    $id = (int) $id;
    if($id<=0){
      $this->status = 1001;
      $this->message = 'ID Kelas tidak valid atau tidak termasuk kedalam sekolah ini';
      $this->__json_out($data);
      die();
    }

    //sanitize post
    $this->__sanitizePost();

    //collection input
    $nama = $this->input->post("nama");
    $is_active = (int) $this->input->post("is_active");

    //input validation
    if(empty($nama)){
      $this->status = 1002;
      $this->message = 'Nama kelas minimal 1 huruf';
      $this->__json_out($data);
      die();
    }

    $kelas = $this->dum->getByIdDanSekolahId($id,$a_sekolah_id);
    if(!isset($kelas->id)){
      $this->status = 1003;
      $this->message = 'Kelas tidak ditemukan atau mungkin sudah dihapus';
      $this->__json_out($data);
      die();
    }

    //lakukan proses penghapusan
    $res = $this->dum->delByIdSekolahId($a_sekolah_id,$id);
    if($res){
      $this->status = 200;
      $this->message = 'Berhasil';
    }else{
      $this->status = 1003;
      $this->message = 'Failed updating to database';
    }
    $this->__json_out($data);
  }
  public function mata_pelajaran($c_matapelajaran_id=""){
    $d = $this->__init();
    $data = new stdClass();
    if(!$this->user_login){
      $this->status = 400;
      $this->message = 'Harus login';
      header("HTTP/1.0 400 Harus login");
      $this->__json_out($data);
      die();
    }
    $pengguna = $d['sess']->user;
    $a_sekolah_id = $pengguna->a_sekolah_id;

    $c_matapelajaran_id =  (int) $c_matapelajaran_id;
    if($c_matapelajaran_id<=0){
      $this->status = 998;
      $this->message = 'Mata Pelajaran ID tidak sah';
      $this->__json_out($data);
      die();
    }

    //ambil data
    $this->status = 200;
    $this->message = 'Berhasil';
    $data = $this->dum->getByGuruIdMataPelajaranId($pengguna->id,$c_matapelajaran_id);

    $this->__json_out($data);
  }
}
