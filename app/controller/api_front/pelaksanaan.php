<?php
class Pelaksanaan extends JI_Controller {
  public function __construct(){
    parent::__construct();
    $this->setTheme("front/");
    $this->load("front/a_sekolah_model",'asm');
    $this->load("front/b_guru_model",'bgm');
    $this->load("api_front/c_kelas_model",'ckm');
    $this->load("api_front/d_siswa_model",'dsm');
    $this->load("api_front/f_pelaksanaan_model",'fpm');
    $this->page_current = 'pelaksanaan';
  }
  private function __sanitizePost($excepts=array()){
    foreach($_POST as $key=>&$val){
      if(is_string($val)){
        if(in_array($key,$excepts)){
          $val = $this->seme_purifier->richtext($val);
        }else{
          $val = $this->__f($val);
        }
      }
    }
  }
  public function index(){
    $d = $this->__init();
    $data = array();
    if(!$this->user_login){
      $this->status = 400;
      $this->message = 'Harus login';
      header("HTTP/1.0 400 Harus login");
      $this->__json_out($data);
      die();
    }
    $pengguna = $d['sess']->user;
    $a_sekolah_id = $pengguna->a_sekolah_id;

    $tbl_as = $this->dsm->getTableAlias();
    $tbl2_as = $this->dsm->getTableAlias2();

    $draw = $this->input->post("draw");
    $sval = $this->input->post("search");
    $sSearch = $this->input->request("sSearch");
    $sEcho = $this->input->post("sEcho");
    $page = $this->input->post("iDisplayStart");
    $pagesize = $this->input->request("iDisplayLength");

    $iSortCol_0 = $this->input->post("iSortCol_0");
    $sSortDir_0 = $this->input->post("sSortDir_0");

    $sortCol = "date";
    $sortDir = strtoupper($sSortDir_0);
    if(empty($sortDir)) $sortDir = "DESC";
    if(strtolower($sortDir) != "desc"){
      $sortDir = "ASC";
    }

    switch($iSortCol_0){
      case 0:
        $sortCol = "$tbl_as.id";
        break;
      case 1:
        $sortCol = "COALESCE($tbl2_as.nama,'-') $sortDir, nourut";
        $sortDir = 'ASC';
        break;
      case 2:
        $sortCol = "COALESCE($tbl2_as.nama,'-') ASC, nourut";
        break;
      case 3:
        $sortCol = "$tbl_as.nama";
        break;
      case 4:
        $sortCol = "$tbl_as.is_active";
        break;
      default:
        $sortCol = "$tbl_as.id";
    }

    if(empty($draw)) $draw = 0;
    if(empty($pagesize)) $pagesize=10;
    if(empty($page)) $page=0;
    $keyword = $sSearch;

    $this->status = '100';
    $this->message = 'Berhasil';
    $dcount = $this->fpm->countBySekolahId($a_sekolah_id,$keyword);
    $ddata = $this->fpm->getBySekolahId($a_sekolah_id,$page,$pagesize,$sortCol,$sortDir,$keyword);

    foreach($ddata as &$gd){
      if(isset($gd->is_active)){
        if(!empty($gd->is_active)){
          $gd->is_active = '<span class="label label-success">aktif</span>';
        }else{
          $gd->is_active = '<span class="label label-default">tidak aktif</span>';
        }
      }
    }
    //sleep(3);
    $another = array();
    $this->__jsonDataTable($ddata,$dcount);
  }
  public function detail($id){
    $d = $this->__init();
    $data = new stdClass();
    if(!$this->user_login){
      $this->status = 400;
      $this->message = 'Harus login';
      header("HTTP/1.0 400 Harus login");
      $this->__json_out($data);
      die();
    }
    $pengguna = $d['sess']->user;
    $a_sekolah_id = $pengguna->a_sekolah_id;

    $id = (int) $id;
    if($id<=0){
      $this->status = 1001;
      $this->message = 'ID Pelaksanaan Ujian tidak valid atau tidak termasuk kedalam sekolah ini';
      $this->__json_out($data);
      die();
    }
    $data = $this->fpm->getByIdGuruId($pengguna->id,$id);
    if(!isset($data->id)){
      $this->status = 1017;
      $this->message = 'ID Pelaksanaan Ujian tidak valid atau tidak termasuk kedalam sekolah ini';
      $this->__json_out($data);
      die();
    }

    $this->status = 200;
    $this->message = 'Berhasil';
    $this->__json_out($data);
  }
  public function tambah(){
    $d = $this->__init();
    $data = new stdClass();
    if(!$this->user_login){
      $this->status = 400;
      $this->message = 'Harus login';
      header("HTTP/1.0 400 Harus login");
      $this->__json_out($data);
      die();
    }
    $pengguna = $d['sess']->user;
    $a_sekolah_id = $pengguna->a_sekolah_id;

    //sanitize post
    $this->__sanitizePost();

    //collection input
    $c_kelas_id = (int) $this->input->post("c_kelas_id");
    $d_ujian_id = (int) $this->input->post("d_ujian_id");
    $sdate = $this->input->post("sdate");
    $edate = $this->input->post("edate");
    $sdate_time = $this->input->post("sdate_time");
    $edate_time = $this->input->post("edate_time");
    $pelaksanaan_status = "belum_dimulai";

    //merge date with time
    $sdate = $sdate.' '.$sdate_time;
    $edate = $edate.' '.$edate_time;

    //input validation
    if($d_ujian_id<=0){
      $this->status = 1016;
      $this->message = 'Ujian ID tidak boleh kosong';
      $this->__json_out($data);
      die();
    }

    //input validation
    if($c_kelas_id<=0){
      $this->status = 1015;
      $this->message = 'Pilih kelas terlebih dahulu';
      $this->__json_out($data);
      die();
    }

    $di = array();
    $di['b_guru_id'] = $pengguna->id;
    $di['c_kelas_id'] = $c_kelas_id;
    $di['d_ujian_id'] = $d_ujian_id;
    $di['cdate'] = "NOW()";
    $di['sdate'] = $sdate;
    $di['edate'] = $edate;
    $di['pelaksanaan_status'] = $pelaksanaan_status;
    $res = $this->fpm->set($di);
    if($res){
      $this->status = 200;
      $this->message = 'Berhasil';
    }else{
      $this->status = 1003;
      $this->message = 'Failed inserting to database';
    }
    $this->__json_out($data);
  }
  public function edit($id){
    $d = $this->__init();
    $data = new stdClass();
    if(!$this->user_login){
      $this->status = 400;
      $this->message = 'Harus login';
      header("HTTP/1.0 400 Harus login");
      $this->__json_out($data);
      die();
    }
    $pengguna = $d['sess']->user;
    $a_sekolah_id = $pengguna->a_sekolah_id;

    $id = (int) $id;
    if($id<=0){
      $this->status = 1001;
      $this->message = 'ID Kelas tidak valid atau tidak termasuk kedalam sekolah ini';
      $this->__json_out($data);
      die();
    }

    //sanitize post
    $this->__sanitizePost();

    //collection input
    $c_kelas_id = (int) $this->input->post("c_kelas_id");
    $nourut = (int) $this->input->post("nourut");
    $nama = $this->input->post("nama");
    $is_active = (int) $this->input->post("is_active");

    //input validation
    if(empty($nama)){
      $this->status = 1002;
      $this->message = 'Nama kelas minimal 1 huruf';
      $this->__json_out($data);
      die();
    }
    if($c_kelas_id<=0) $c_kelas_id = "null";
    if($nourut<=0){
      $nourut = $this->dsm->getNoUrut($a_sekolah_id,$c_kelas_id);
    }

    $kelas = $this->dsm->getByIdDanSekolahId($id,$a_sekolah_id);
    if(!isset($kelas->id)){
      $this->status = 1003;
      $this->message = 'Kelas tidak ditemukan atau mungkin sudah dihapus';
      $this->__json_out($data);
      die();
    }

    $du = array();
    $du['c_kelas_id'] = $c_kelas_id;
    $du['nourut'] = $nourut;
    $du['nama'] = $nama;
    $du['is_active'] = $is_active;
    $res = $this->dsm->updateByIdSekolahId($a_sekolah_id,$id,$du);
    if($res){
      $this->status = 200;
      $this->message = 'Berhasil';
    }else{
      $this->status = 1003;
      $this->message = 'Failed updating to database';
    }
    $this->__json_out($data);
  }
  public function hapus($id){
    $d = $this->__init();
    $data = new stdClass();
    if(!$this->user_login){
      $this->status = 400;
      $this->message = 'Harus login';
      header("HTTP/1.0 400 Harus login");
      $this->__json_out($data);
      die();
    }
    $pengguna = $d['sess']->user;
    $a_sekolah_id = $pengguna->a_sekolah_id;

    $id = (int) $id;
    if($id<=0){
      $this->status = 1001;
      $this->message = 'ID Kelas tidak valid atau tidak termasuk kedalam sekolah ini';
      $this->__json_out($data);
      die();
    }

    //sanitize post
    $this->__sanitizePost();

    //collection input
    $nama = $this->input->post("nama");
    $is_active = (int) $this->input->post("is_active");

    //input validation
    if(empty($nama)){
      $this->status = 1002;
      $this->message = 'Nama kelas minimal 1 huruf';
      $this->__json_out($data);
      die();
    }

    $kelas = $this->dsm->getByIdDanSekolahId($id,$a_sekolah_id);
    if(!isset($kelas->id)){
      $this->status = 1003;
      $this->message = 'Kelas tidak ditemukan atau mungkin sudah dihapus';
      $this->__json_out($data);
      die();
    }

    //lakukan proses penghapusan
    $res = $this->dsm->delByIdSekolahId($a_sekolah_id,$id);
    if($res){
      $this->status = 200;
      $this->message = 'Berhasil';
    }else{
      $this->status = 1003;
      $this->message = 'Failed updating to database';
    }
    $this->__json_out($data);
  }
  public function dimulai($id){
    $d = $this->__init();
    $data = new stdClass();
    if(!$this->user_login){
      $this->status = 400;
      $this->message = 'Harus login';
      header("HTTP/1.0 400 Harus login");
      $this->__json_out($data);
      die();
    }
    $pengguna = $d['sess']->user;
    $a_sekolah_id = $pengguna->a_sekolah_id;

    $id = (int) $id;
    if($id<=0){
      $this->status = 1001;
      $this->message = 'ID Pelaksanaan Ujian tidak valid atau tidak termasuk kedalam sekolah ini';
      $this->__json_out($data);
      die();
    }
    $data = $this->fpm->getByIdGuruId($pengguna->id,$id);
    if(!isset($data->id)){
      $this->status = 1017;
      $this->message = 'ID Pelaksanaan Ujian tidak valid atau tidak termasuk kedalam sekolah ini';
      $this->__json_out($data);
      die();
    }
    $res = $this->fpm->update($id,array("pelaksanaan_status"=>'dimulai'));
    if($res){
      $this->status = 200;
      $this->message = 'Berhasil';
    }else{
      $this->status = 1017;
      $this->message = 'Gagal merubah status';
    }
    $this->__json_out($data);
  }

  public function dijeda($id){
    $d = $this->__init();
    $data = new stdClass();
    if(!$this->user_login){
      $this->status = 400;
      $this->message = 'Harus login';
      header("HTTP/1.0 400 Harus login");
      $this->__json_out($data);
      die();
    }
    $pengguna = $d['sess']->user;
    $a_sekolah_id = $pengguna->a_sekolah_id;

    $id = (int) $id;
    if($id<=0){
      $this->status = 1001;
      $this->message = 'ID Pelaksanaan Ujian tidak valid atau tidak termasuk kedalam sekolah ini';
      $this->__json_out($data);
      die();
    }
    $data = $this->fpm->getByIdGuruId($pengguna->id,$id);
    if(!isset($data->id)){
      $this->status = 1017;
      $this->message = 'ID Pelaksanaan Ujian tidak valid atau tidak termasuk kedalam sekolah ini';
      $this->__json_out($data);
      die();
    }
    $res = $this->fpm->update($id,array("pelaksanaan_status"=>'dijeda'));
    if($res){
      $this->status = 200;
      $this->message = 'Berhasil';
    }else{
      $this->status = 1017;
      $this->message = 'Gagal merubah status';
    }
    $this->__json_out($data);
  }
  public function dihentikan($id){
    $d = $this->__init();
    $data = new stdClass();
    if(!$this->user_login){
      $this->status = 400;
      $this->message = 'Harus login';
      header("HTTP/1.0 400 Harus login");
      $this->__json_out($data);
      die();
    }
    $pengguna = $d['sess']->user;
    $a_sekolah_id = $pengguna->a_sekolah_id;

    $id = (int) $id;
    if($id<=0){
      $this->status = 1001;
      $this->message = 'ID Pelaksanaan Ujian tidak valid atau tidak termasuk kedalam sekolah ini';
      $this->__json_out($data);
      die();
    }
    $data = $this->fpm->getByIdGuruId($pengguna->id,$id);
    if(!isset($data->id)){
      $this->status = 1017;
      $this->message = 'ID Pelaksanaan Ujian tidak valid atau tidak termasuk kedalam sekolah ini';
      $this->__json_out($data);
      die();
    }
    $res = $this->fpm->update($id,array("pelaksanaan_status"=>'selesai'));
    if($res){
      $this->status = 200;
      $this->message = 'Berhasil';
    }else{
      $this->status = 1017;
      $this->message = 'Gagal merubah status';
    }
    $this->__json_out($data);
  }
}
