<?php
class Lupa extends JI_Controller {
  var $is_email = 1;

  public function __construct(){
    parent::__construct();
    $this->setTheme("front/");
    $this->lib("seme_purifier");
    $this->load("front/a_sekolah_model","asm");
    $this->load("front/b_guru_model","bgm");
    $this->page_current = 'lupa';
  }

	private function __passwordGenerateLink($user_id){
		$this->lib("conumtext");
		$token = $this->conumtext->genRand($type="str",$min=18,$max=24);
		$this->bgm->update($user_id,array("api_web_token"=>$token));
		return base_url('account/password/reset/'.$token);
	}

  public function index(){
    $data = $this->__init();
    if($this->user_login){
      redir(base_url("dashboard"),0,0);
      die();
    }
    $data['notif'] = '';
    $data['berhasil'] = 0;

		foreach($_POST as $key=>&$val){
			if(is_string($val)){
				if($key == 'deskripsi'){
					$val = $this->seme_purifier->richtext($val);
				}else{
					$val = $this->__f($val);
				}
			}
		}

    $email = $this->input->post("email");
    if(strlen($email)>4){
      $email = strtolower($email);
      $bgm = $this->bgm->check($email);
      if(isset($bgm->id)){
        if(empty($bgm->is_active)){
          $data['notif'] = 'Profil anda sudah tidak aktif';
        }else{
          if($this->is_email){
            //load library
            $this->lib('seme_email');

            //building content replacer
  					$replacer = array();
  					$replacer['fnama'] = $bgm->nama;
  					$replacer['site_name'] = $this->site_name;
  					$replacer['site_name1'] = $this->site_name;
  					$replacer['reset_link'] = $this->__passwordGenerateLink($bgm->id);

            //building email properties
  					$this->seme_email->replyto($this->site_name,$this->site_replyto);
  					$this->seme_email->from($this->site_email,$this->site_name);
  					$this->seme_email->subject('Lupa Password');
  					$this->seme_email->to($bgm->email,$bgm->nama);
  					$this->seme_email->template('account_forgot');
  					$this->seme_email->replacer($replacer);
  					$this->seme_email->send();
          }
          $data['notif'] = 'Email telah terkirim, silakan periksa di kotak masuk atau folder spam';
        }

      }else{
        $data['notif'] = 'Email atau password salah';
      }
    }

    $this->setTitle("Lupa Password ".$this->site_suffix);

    $this->__breadCrumb("Lupa");
    $this->putThemeContent("lupa/home",$data);
    $this->putJsContent("lupa/home_bottom",$data);
    $this->loadLayout("col-1",$data);
    $this->render();
  }
}
