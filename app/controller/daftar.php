<?php
class Daftar extends JI_Controller {
  public function __construct(){
    parent::__construct();
    $this->setTheme("front/");
    $this->lib("seme_purifier");
    $this->load("front/a_sekolah_model","asm");
    $this->load("front/b_guru_model","bgm");
    $this->page_current = 'daftar';
  }
  public function index(){
    $data = $this->__init();
    if($this->user_login){
      redir(base_url("dashboard"),0,0);
      die();
    }
    $data['notif'] = '';
    $data['berhasil'] = 0;

		foreach($_POST as $key=>&$val){
			if(is_string($val)){
				if($key == 'deskripsi'){
					$val = $this->seme_purifier->richtext($val);
				}else{
					$val = $this->__f($val);
				}
			}
		}

    $nama = $this->input->post("nama");
    $email = $this->input->post("email");
    $sekolah = $this->input->post("sekolah");
    $password = $this->input->post("password");
    $matapelajaran = $this->input->post("matapelajaran");
    $ulangi_password = $this->input->post("ulangi_password");
    if(strlen($nama)>1 && strlen($email)>4 && strlen($sekolah)>6){
      $nama = ucwords($nama);
      $email = strtolower($email);
      $sekolah = strtoupper($sekolah);
      $matapelajaran = ucwords($matapelajaran);

      $bgm = $this->bgm->check($email);
      if(!isset($bgm->id)){
        if($password != $ulangi_password){
          $data['notif'] = 'Password dengan Konfirmasi Password tidak sama';
        }else{
          $this->bgm->trans_start();
          $asm = $this->asm->check($sekolah);
          $a_sekolah_id = '';
          if(isset($asm->id)){
            $a_sekolah_id = $asm->id;
          }else{
            $di = array();
            $di['nama'] = $sekolah;
            $a_sekolah_id = $this->asm->set($di);
            if($a_sekolah_id){
              $this->bgm->trans_commit();
            }else{
              $this->bgm->trans_rollback();
            }

          }
          $di = array();
          $di['nama'] = $nama;
          $di['email'] = $email;
          $di['a_sekolah_id'] = $a_sekolah_id;
          $di['matapelajaran'] = $matapelajaran;
          $di['password'] = password_hash($password,PASSWORD_BCRYPT);
          $res = $this->bgm->set($di);
          if($res){
            $this->bgm->trans_commit();
            $data['berhasil'] = $res;
          }else{
            $this->bgm->trans_rollback();
          }
          $this->bgm->trans_end();
        }
      }else{
        $data['notif'] = 'Email sudah terdaftar, silakan coba login';
      }
    }
    
    $data['berhasil'] = (int) $data['berhasil'];
    if($data['berhasil']>0){
      $sess = $this->getKey();
      if(!isset($sess->user)) $sess = new stdClass();
      if(!isset($sess->user->id)) $sess->user = new stdClass();
      $sess->user = $this->bgm->getById($data['berhasil']);
      $this->setKey($sess);
      redir(base_url("dashboard"),0,0);
    }

    $this->setTitle("Daftar ".$this->site_suffix);

    $this->__breadCrumb("Daftar");
    $this->putThemeContent("daftar/home",$data);
    $this->putJsContent("daftar/home_bottom",$data);
    $this->loadLayout("col-1",$data);
    $this->render();
  }
}
