<?php
class Home extends JI_Controller {
  public function __construct(){
    parent::__construct();
    $this->setTheme("front/");
    $this->page_current = 'homepage';
  }
  public function index(){
    $data = $this->__init();

    $this->setTitle($this->site_title." ".$this->site_suffix);
    $this->setKeyword($this->site_name);
    $this->setDescription($this->site_description);
    $this->putThemeContent("home/home",$data);
    $this->loadLayout("col-1",$data);
    $this->render();
  }
}
