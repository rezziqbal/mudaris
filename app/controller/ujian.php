<?php
class Ujian extends JI_Controller {
  public function __construct(){
    parent::__construct();
    $this->setTheme("front/");
    $this->load("front/a_sekolah_model",'asm');
    $this->load("front/b_guru_model",'bgm');
    $this->load("front/c_kelas_model",'ckm');
    $this->load("front/c_matapelajaran_model",'cmpm');
    $this->load("front/d_ujian_model",'dum');
    $this->page_current = 'ujian';
  }
  protected function __sifat($sifat){
    switch($sifat){
      case "open" :
        $sifat = 'Open Book';
        break;
      case "close" :
        $sifat = 'Close Book';
        break;
      default: $sifat='hide';
    }
    return $sifat;
  }
  public function index(){
    $data = $this->__init();
    //$this->debug($data);
    if(!$this->user_login){
      redir(base_url("login"),0,0);
      die();
    }
    $pengguna = $data['sess']->user;

    $data['sekolah'] = $this->asm->getById($pengguna->a_sekolah_id);
    $data['kelas'] = $this->ckm->getBySekolahId($pengguna->a_sekolah_id);
    $data['matapelajaran'] = $this->cmpm->getMataPelajaranSaya($pengguna->a_sekolah_id,$pengguna->id);

    //load datatables
    $this->loadCss($this->cdn_url("assets/css/dataTables.bootstrap.min"),"before");
    $this->putJsFooter($this->cdn_url("assets/js/jquery.dataTables.min"));
    $this->putJsFooter($this->cdn_url("assets/js/dataTables.bootstrap.min"));
    //$this->putJsFooter($this->cdn_url("assets/js/responsive.bootstrap.min"));

    $this->setTitle("Ujian ".$this->site_suffix);
    $this->__breadCrumb("Ujian");
    $this->putThemeContent("ujian/home_modal",$data);
    $this->putThemeContent("ujian/home",$data);
		$this->putJsContent("ujian/home_bottom",$data);
    $this->loadLayout("col-1-dashboard",$data);
    $this->render();
  }
  public function edit_soal($id){
    $data = $this->__init();
    //$this->debug($data);
    if(!$this->user_login){
      redir(base_url("login"),0,0);
      die();
    }
    $pengguna = $data['sess']->user;

    //check id
    $id = (int) $id;
    if($id<=0){
      redir(base_url("ujian"),0,0);
      die();
    }

    //check ujian
  	$ujian = $this->dum->getByIdGuruId($pengguna->id,$id);
    if(!isset($ujian->id)){
      redir(base_url("ujian"),0,0);
      die();
    }

    $data['sekolah'] = $this->asm->getById($pengguna->a_sekolah_id);
    $data['kelas'] = $this->ckm->getBySekolahId($pengguna->a_sekolah_id);
    $data['matapelajaran'] = $this->cmpm->getMataPelajaranSaya($pengguna->a_sekolah_id,$pengguna->id);
    $matpel = array();
    foreach($data['matapelajaran'] as $mp){
      $matpel[$mp->id] = $mp;
    }
    unset($mp);
    $data['matapelajaran'] = $matpel;
    unset($matpel);
    $data['ujian'] = $ujian;

    //load datatables
    $this->putJsFooter("https://cdn.ckeditor.com/4.11.4/standard/ckeditor.js");
    $this->putJsFooter($this->cdn_url("assets/js/jquery.scrollto.min.js"));
    //$this->putJsFooter($this->cdn_url("assets/js/responsive.bootstrap.min"));

    $this->setTitle("Edit Soal Ujian ".$this->site_suffix);
    $this->__breadCrumb("Ujian",base_url("ujian"));
    $this->__breadCrumb("Edit Soal");
    $this->putThemeContent("ujian/edit_soal_modal",$data);
    $this->putThemeContent("ujian/edit_soal",$data);
		$this->putJsContent("ujian/edit_soal_bottom",$data);
    $this->loadLayout("col-1-dashboard",$data);
    $this->render();
  }
}
