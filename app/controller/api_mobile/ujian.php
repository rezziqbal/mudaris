<?php
class Ujian extends JI_Controller {
  public function __construct(){
    parent::__construct();
    $this->setTheme("front/");
    $this->lib("seme_purifier");
    $this->lib("conumtext");
    $this->load("api_mobile/a_sekolah_model","asm");
    $this->load("api_mobile/b_guru_model","bgm");
    $this->load("api_mobile/c_kelas_model","ckm");
    $this->load("api_mobile/c_matapelajaran_model","cmpm");
    $this->load("api_mobile/d_siswa_model","dswm");
    $this->load("api_mobile/d_ujian_model","dujm");
    $this->load("api_mobile/e_soal_model","esm");
    $this->load("api_mobile/e_soal_pilihan_model","espm");
    $this->load("api_mobile/f_pelaksanaan_model","fpm");
  }

	private function __activateMobileToken($user_id){
		$user_id = (int) $user_id;
		$this->lib("conumtext");
		$token = $this->conumtext->genRand($type="str",$min=6,$max=24);
		if($user_id == 2) $token = 'KMZDR';
		if($user_id == 1) $token = 'KMZDS';
		return $token;
	}

  public function index($a_sekolah_id=""){
    $dt = $this->__init();

		//default result
		$data = array();
		$data['ujian'] = new stdClass();
    $data['soal'] = array();
    $data['jawaban'] = array();

		//check apikey
		$apikey = $this->input->get('apikey');
		$c = $this->apikey_check($apikey);
		if(!$c){
			$this->status = 102;
			$this->message = 'Missing or invalid apikey';
			$this->__json_out($data);
			die();
		}
		$apisess = $this->input->get('apisess');
    if(strlen($apisess)<=4){
			$this->status = 103;
			$this->message = 'Missing or invalid apisess';
			$this->__json_out($data);
			die();
    }

		$siswa = $this->dswm->getByToken($apisess,"api_mobile_token");
		if(!isset($siswa->id)){
			$this->status = 108;
			$this->message = 'Token salah, silakan login ulang';
			$this->__json_out($data);
			die();
		}

    $a_sekolah_id = (int) $this->input->get("a_sekolah_id");
		if($a_sekolah_id<=0){
			$this->status = 102;
			$this->message = 'Invalid A_Sekolah_Id';
			$this->__json_out($data);
			die();
		}

		$sekolah = $this->asm->getById($a_sekolah_id);
		if(!isset($sekolah->id)){
			$this->status = 106;
			$this->message = 'Sekolah Not found';
			$this->__json_out($data);
			die();
		}

    $c_kelas_id = (int) $this->input->get("c_kelas_id");
		if($c_kelas_id<=0){
			$this->status = 102;
			$this->message = 'Invalid c_kelas_id';
			$this->__json_out($data);
			die();
		}

    $c_matapelajaran_id = (int) $this->input->get("c_matapelajaran_id");
		if($c_matapelajaran_id<=0){
			$this->status = 113;
			$this->message = 'Invalid c_matapelajaran_id';
			$this->__json_out($data);
			die();
		}

		$kelas = $this->ckm->getByIdSekolahId($a_sekolah_id, $c_kelas_id);
		if(!isset($kelas->id)){
			$this->status = 105;
			$this->message = 'Kelas belum tersedia disekolah ini';
			$this->__json_out($data);
			die();
		}

    $ujian = $this->fpm->getByKelasIdMataPelajaranId($c_kelas_id,$c_matapelajaran_id);

    $pilihans = array();
    //$pilihan = array();
    $pilihan = $this->espm->getByUjianID($ujian->d_ujian_id);
    foreach($pilihan as $pil){
      $id = (int) $pil->e_soal_id;
      if(!isset($pilihans[$id])) $pilihans[$id] = array();
      $pilihans[$id][] = $pil;
    }
    unset($pil);
    unset($pilihan);

    $soal = $this->esm->getByUjianID($ujian->d_ujian_id);
    foreach($soal as &$sl){
      $sl->pilihan = array();
      if(isset($pilihans[$sl->id])) $sl->pilihan = $pilihans[$sl->id];
    }

    $this->status = 200;
    $this->message = 'Berhasil';
    $data['ujian'] = $ujian;
    $data['soal'] = $soal;
    $this->__json_out($data);
  }
}
