<?php
class Sekolah extends JI_Controller {
  public function __construct(){
    parent::__construct();
    $this->setTheme("front/");
    $this->lib("seme_purifier");
    $this->load("api_mobile/a_sekolah_model","asm");
    $this->load("api_mobile/b_guru_model","bgm");
  }
  public function index(){
    $dt = $this->__init();

		//default result
		$data = array();
		$data['sekolah'] = array();

		//check apikey
		$apikey = $this->input->get('apikey');
		$c = $this->apikey_check($apikey);
		if(!$c){
			$this->status = 102;
			$this->message = 'Missing or invalid apikey';
			$this->__json_out($data);
			die();
		}
    $this->status = 200;
    $this->message = 'Berhasil';
    $keyword = $this->input->request("keyword");
    if(strlen($keyword)<=1) $keyword = '';
    $data['sekolah'] = $this->asm->search($keyword);
    $this->__json_out($data);
  }
}
