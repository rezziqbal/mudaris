<?php
class Login extends JI_Controller {
  public function __construct(){
    parent::__construct();
    $this->setTheme("front/");
    $this->lib("seme_purifier");
    $this->lib("conumtext");
    $this->load("api_mobile/a_sekolah_model","asm");
    $this->load("api_mobile/b_guru_model","bgm");
    $this->load("api_mobile/c_kelas_model","ckm");
    $this->load("api_mobile/c_matapelajaran_model","cmpm");
    $this->load("api_mobile/d_siswa_model","dswm");
  }


	private function __activateMobileToken($user_id){
		$user_id = (int) $user_id;
		$this->lib("conumtext");
		$token = $this->conumtext->genRand($type="str",$min=6,$max=24);
		if($user_id == 2) $token = 'KMZDR';
		if($user_id == 1) $token = 'KMZDS';
		return $token;
	}

  public function index($a_sekolah_id=""){
    $dt = $this->__init();

		//default result
		$data = array();
		$data['siswa'] = new stdClass();
		$data['matapelajaran'] = array();

		//check apikey
		$apikey = $this->input->get('apikey');
		$c = $this->apikey_check($apikey);
		if(!$c){
			$this->status = 102;
			$this->message = 'Missing or invalid apikey';
			$this->__json_out($data);
			die();
		}

    $a_sekolah_id = (int) $this->input->get("a_sekolah_id");
		if($a_sekolah_id<=0){
			$this->status = 102;
			$this->message = 'Invalid A_Sekolah_Id';
			$this->__json_out($data);
			die();
		}

		$sekolah = $this->asm->getById($a_sekolah_id);
		if(!isset($sekolah->id)){
			$this->status = 102;
			$this->message = 'Sekolah Not found';
			$this->__json_out($data);
			die();
		}

    $c_kelas_id = (int) $this->input->get("c_kelas_id");
		if($c_kelas_id<=0){
			$this->status = 102;
			$this->message = 'Invalid c_kelas_id';
			$this->__json_out($data);
			die();
		}

		$kelas = $this->ckm->getByIdSekolahId($a_sekolah_id, $c_kelas_id);
		if(!isset($kelas->id)){
			$this->status = 102;
			$this->message = 'Kelas belum tersedia disekolah ini';
			$this->__json_out($data);
			die();
		}

    $nama = $this->input->request("nama");
    if(strlen($nama)<=1){
			$this->status = 103;
			$this->message = 'Nama tidak valid, silakan isikan nama yang benar';
			$this->__json_out($data);
			die();
    }

    //check nama
    $siswa = $this->dswm->check($a_sekolah_id,$c_kelas_id,$nama);
    if(!isset($siswa->api_mobile_token)){
      $di = array();
      $di['a_sekolah_id'] = $a_sekolah_id;
      $di['c_kelas_id'] = $c_kelas_id;
      $di['nama'] = $nama;
      $res = $this->dswm->set($di);
      if(!$res){
  			$this->status = 104;
  			$this->message = 'Gagal mendaftarkan siswa ke database';
  			$this->__json_out($data);
        die();
      }
      $siswa = $this->dswm->getById($res);
    }

    //matapelajaran
    $matapelajaran = $this->cmpm->getBySekolahId($a_sekolah_id);

    //build update
    $du = array();
    $du['api_mobile_token'] = $this->__activateMobileToken($siswa->id);
    $this->dswm->update($a_sekolah_id, $c_kelas_id, $siswa->id, $du);
    $siswa->api_mobile_token = $du['api_mobile_token'];

    //output respon
    $this->status = 200;
    $this->message = 'Berhasil';
    $data['siswa'] = $siswa;
    $data['matapelajaran'] = $matapelajaran;
    $this->__json_out($data);
  }
}
