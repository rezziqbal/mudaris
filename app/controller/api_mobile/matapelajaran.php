<?php
class MataPelajaran extends JI_Controller {
  public function __construct(){
    parent::__construct();
    $this->setTheme("front/");
    $this->lib("seme_purifier");
    $this->load("api_mobile/a_sekolah_model","asm");
    $this->load("api_mobile/b_guru_model","bgm");
    $this->load("api_mobile/c_kelas_model","ckm");
    $this->load("api_mobile/c_matapelajaran_model","cmpm");
  }
  public function index($a_sekolah_id=""){
    $dt = $this->__init();

		//default result
		$data = array();
		$data['matapelajaran'] = array();

		//check apikey
		$apikey = $this->input->get('apikey');
		$c = $this->apikey_check($apikey);
		if(!$c){
			$this->status = 102;
			$this->message = 'Missing or invalid apikey';
			$this->__json_out($data);
			die();
		}

    $a_sekolah_id = (int) $this->input->get("a_sekolah_id");
		if($a_sekolah_id<=0){
			$this->status = 102;
			$this->message = 'Invalid A_Sekolah_Id';
			$this->__json_out($data);
			die();
		}

		$sekolah = $this->asm->getById($a_sekolah_id);
		if(!isset($sekolah->id)){
			$this->status = 102;
			$this->message = 'Sekolah Not found';
			$this->__json_out($data);
			die();
		}

    $this->status = 200;
    $this->message = 'Berhasil';

    $data['matapelajaran'] = $this->cmpm->getBySekolahId($a_sekolah_id);
    $this->__json_out($data);
  }
}
