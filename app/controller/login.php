<?php
class Login extends JI_Controller {
  public function __construct(){
    parent::__construct();
    $this->setTheme("front/");
    $this->lib("seme_purifier");
    $this->load("front/a_sekolah_model","asm");
    $this->load("front/b_guru_model","bgm");
    $this->page_current = 'login';
  }
  public function index(){
    $data = $this->__init();
    if($this->user_login){
      redir(base_url("dashboard"),0,0);
      die();
    }
    $data['notif'] = '';
    $data['berhasil'] = 0;

		foreach($_POST as $key=>&$val){
			if(is_string($val)){
				if($key == 'deskripsi'){
					$val = $this->seme_purifier->richtext($val);
				}else{
					$val = $this->__f($val);
				}
			}
		}

    $email = $this->input->post("email");
    $password = $this->input->post("password");
    if(strlen($email)>4 && strlen($password)>4){
      $email = strtolower($email);
      $bgm = $this->bgm->check($email);
      if(isset($bgm->id)){
        if(!empty($bgm->is_active)){
          if($bgm->password == md5($password)){
            $bgm->password = password_hash($password,PASSWORD_BCRYPT);
            $du = array("password"=>$bgm->password);
            $this->bgm->update($bgm->id,$du);
          }
          if(password_verify($password,$bgm->password)){
            $sess = $this->getKey();
            if(!isset($sess->user)) $sess = new stdClass();
            $sess->user = $bgm;
            $this->setKey($sess);
            redir(base_url("dashboard"),0,0);
            die();
          }else{
            $data['notif'] = 'Email atau password salah';
          }
        }else{
          $data['notif'] = 'Profil anda sudah tidak aktif';
        }

      }else{
        $data['notif'] = 'Email atau password salah';
      }
    }

    $this->setTitle("Login ".$this->site_suffix);

    $this->__breadCrumb("Login");
    $this->putThemeContent("login/home",$data);
    $this->putJsContent("login/home_bottom",$data);
    $this->loadLayout("col-1",$data);
    $this->render();
  }
}
