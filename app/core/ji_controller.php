<?php
	Class JI_Controller extends SENE_Controller {
		var $site_name = 'Mudaris';
		var $site_tagline = 'Aplikasi yang mudah dan praktis untuk melaksanakan ujian';
		var $site_name_admin = 'SellOn Online Marketplace';
		var $site_version = '0.0.1';
		var $site_title = 'Mudaris Aplikasi Ujian yang mudah dan praktis untuk SD, SMP, SMA, dan Non Formal';
		var $site_description = 'Mudaris merupakan aplikasi untuk ujian, quiz, ulangan, UTS dan atau UAS yang Mudah dan Praktis';
		var $site_email = 'hi@somein.co.id';
		var $site_replyto = 'hi@somein.co.id';
		var $site_phone = '085861624300';
		var $site_suffix = ' | Mudaris: Mudah dan Praktis';
		var $site_suffix_admin = ' - Admin Mudaris';
		var $site_keyword = 'SellOn';
		var $page_current = 'beranda';
		var $menu_current = 'beranda';
		var $site_author = 'Solusi Mega Indonesia';
		var $site_company = 'CV. Solusi Mega Indonesia';
		var $site_address = 'Jl Soreang-Padasuka No 3 Citiru, Kabupaten Bandung, Jawa Barat, Indonesia 40911';
		var $user_login = 0;
		var $admin_login = 0;
		var $status = 404;
		var $message = 'Error, not found!';
		var $breadcrumbs;

		var $company_name = 'Solusi Mega Indonesia';
		var $company_url = 'https://www.somein.co.id/';
		var $company_address = 'Kutawaringin, Kab Bandung, Jawa Barat, Indonesia';

		var $site_social_email = 'hi@somein.co.id';
		var $site_social_fb = '';
		var $site_social_ig = 'https://www.instagram.com/somein.co.id/';
		var $site_social_youtube = '';
		var $site_social_twitter = 'https://twitter.com/drosanda';

		var $skins;
		var $skin_admin_logo = 'skin/admin/img/logo-sellon.png';
		var $skin_front_logo = '';

		var $cms_blog = 'media/blog';
		var $media_icon = 'media/icon/';
		var $media_user = 'media/user/';
		var $media_produk = 'media/produk/';
		var $media_pengguna = 'media/pengguna/';
		var $media_campaign = 'media/campaign/';
		var $media_produk_thumb = 'media/produk/thumb/';

		var $fcm_server_token = 'AIzaSyBr0fD8PcizYTgPX908Z_sYS87jKZn5IE0';

		var $site_environment = 'development';
		var $cdn_url = '';
		var $is_strict_module = 0;
		var $current_parent = '';
		var $current_page = '';

		public function apikey_check($apikey){
			if(strlen($apikey)<7) return 0;
			$this->load("api_mobile/a_apikey_model","ji_aakm");
			return $this->ji_aakm->countByCode($apikey);
		}

		public function nation_check($nation_code){
			if(!empty($nation_code) && (strlen($nation_code)>0 && strlen($nation_code)<=5)){
				return (int) $nation_code;
			}else{
				return 0;
			}
		}

		public function __json_out($dt){
			$this->lib('sene_json_engine','sene_json');
			$data = array();
			$data["status"]  = (int) $this->status;
			$data["message"] = $this->message;
			$data["data"]  = $dt;
			$this->sene_json->out($data);
			die();
		}

		public function __breadCrumb($name="home",$url="#",$title=""){
			$bc = new stdClass();
			$bc->name = $name;
			$bc->url = $url;
			$bc->title = $title;
			$this->breadcrumbs[] = $bc;
		}
		private function __menuBuilder($menus){
			$mn1 = array();
			$mn2 = array();
			$mn3 = array();
			$mn4 = array();
			$menu = array();
			foreach($menus as $mn){
				$posisi = $mn->posisi;
				if($mn->utype == 'main_menu'){
					if(!isset($mn1[$posisi])) $mn1[$posisi] = array();
					if(!isset($mn1[$posisi][$mn->id])) $mn1[$posisi][$mn->id] = array();
					$mn1[$mn->posisi][$mn->id] = $mn;
				}else if($mn->utype == 'sub_menu'){
					if(!isset($mn2[$posisi])) $mn2[$posisi] = array();
					if(!isset($mn2[$posisi][$mn->b_menu_id])) $mn2[$posisi][$mn->b_menu_id] = array();
					if(!isset($mn2[$posisi][$mn->b_menu_id])) $mn2[$posisi][$mn->b_menu_id][$mn->id] = new stdClass();
					$mn2[$posisi][$mn->b_menu_id][$mn->id] = $mn;
				}else if($mn->utype == 'sub_sub_menu'){
					if(!isset($mn3[$posisi])) $mn3[$posisi] = array();
					if(!isset($mn3[$posisi][$mn->b_menu_id])) $mn3[$posisi][$mn->b_menu_id] = array();
					if(!isset($mn3[$posisi][$mn->b_menu_id])) $mn3[$posisi][$mn->b_menu_id][$mn->id] = new stdClass();
					$mn3[$posisi][$mn->b_menu_id][$mn->id] = $mn;
				}else if($mn->utype == 'sub_sub_sub_menu'){
					if(!isset($mn4[$posisi])) $mn4[$posisi] = array();
					if(!isset($mn4[$posisi][$mn->b_menu_id])) $mn4[$posisi][$mn->b_menu_id] = array();
					if(!isset($mn4[$posisi][$mn->b_menu_id])) $mn4[$posisi][$mn->b_menu_id][$mn->id] = new stdClass();
					$mn4[$posisi][$mn->b_menu_id][$mn->id] = $mn;
				}
			}
			foreach($mn1 as $km=>$vm){
				foreach($vm as &$m1){
					$m1->childs = array();
					if(isset($mn2[$km][$m1->id])) $m1->childs = $mn2[$km][$m1->id];
					foreach($m1->childs as &$m2){
						$m2->childs = array();
						if(isset($mn3[$km][$m2->id])) $m2->childs = $mn3[$km][$m2->id];
						foreach($m2->childs as &$m3){
							$m3->childs = array();
							if(isset($mn4[$km][$m3->id])) $m3->childs = $mn4[$km][$m3->id];
						}
					}
				}
			}
			return $mn1;
		}

		public function __construct(){
			parent::__construct();
			$this->breadcrumbs = array();
			$this->skins = new stdClass();
			$this->skins->front = base_url('skin/front/');
			$this->skins->homepage = base_url('skin/homepage/');
			$this->skins->admin = base_url('skin/admin/');
		}
		public function __init(){
			//$this->load('front/b_kategori_model','bk');
			$data = array();
			$sess = $this->getKey();
			if(!is_object($sess)) $sess = new stdClass();
			if(!isset($sess->user)) $sess->user = new stdClass();
			if(isset($sess->user->id)) $this->user_login = 1;

			if(!isset($sess->admin)) $sess->admin = new stdClass();
			if(isset($sess->admin->id)) $this->admin_login = 1;

			$data['sess'] = $sess;
			$data['site_title'] = $this->site_title;
			$data['site_description'] = $this->site_description;
			$data['page_current'] = $this->page_current;
			$data['menu_current'] = $this->menu_current;
			$data['site_author'] = $this->site_author;
			$data['site_keyword'] = $this->site_keyword;
			$data['user_login'] = $this->user_login;
			$data['admin_login'] = $this->admin_login;
			$data['skins'] = $this->skins;

			$this->load("front/b_menu_model","bmn");
			$menu = array();
			$data['menu_main'] = array();
			$data['menu_mobile'] = array();
			$data['menu_footer_1'] = array();
			$data['menu_footer_2'] = array();
			$menu = $this->__menuBuilder($this->bmn->getActive());
			if(isset($menu['main'])){
				$data['menu_main'] = $menu['main'];
				$data['menu_mobile'] = $menu['main'];
			}
			if(isset($menu['mobile'])) $data['menu_mobile'] = $menu['mobile'];
			if(isset($menu['footer_1'])) $data['menu_footer_1'] = $menu['footer_1'];
			if(isset($menu['footer_2'])) $data['menu_footer_2'] = $menu['footer_2'];

			$this->setTitle($this->site_title);
			$this->setDescription($this->description);
			$this->setRobots('INDEX,FOLLOW');
			$this->setAuthor($this->site_author);
			$this->setKeyword($this->site_keyword);
			$this->setIcon($this->cdn_url('favicon.png'));
			$this->setShortcutIcon($this->cdn_url('favicon.png'));
			return $data;
		}
		public function __jsonDataTable($data,$count,$another=array()){
			$this->lib('sene_json_engine','sene_json');
			$rdata = array();
			if(!is_array($data)) $data = array();
			$dt1 = array();
			$dt2 = array();
			if(!is_array($data)){
				trigger_error('jsonDataTable first params need array!');
				die();
			}
			foreach($data as $dat){
				$dt2 = array();
				if(is_int($dat)) trigger_error('[ERROR: '.$dat.'] Data table not well performed because a query execution error!');
				foreach($dat as $dt){
					$dt2[] = $dt;
				}
				$dt1[] = $dt2;
			}

			if(is_array($another)) $rdata = $another;
			$rdata['data'] = $dt1;
			$rdata['recordsFiltered'] = $count;
			$rdata['recordsTotal'] = $count;
			$rdata['status'] = (int) $this->status;
			$rdata['message'] = $this->message;
			$this->sene_json->out($rdata);
			die();
		}

		public function __bankHtml($utype){
			$b = new stdClass();
			$b->nama = $utype;
			$b->norek = '';
			$b->an = '';
			switch($utype){
				case 'transfer_bca_1':
					$b->nama = 'BCA';
					$b->norek = '7750344563';
					$b->an = 'Daeng Rosanda';
					break;
				case 'transfer_mandiri_1':
					$b->nama = 'Mandiri';
					$b->norek = '1300011142505';
					$b->an = 'Daeng Rosanda';
					break;
				case 'transfer_bni_1':
					$b->nama = 'BNI';
					$b->norek = '0152948767';
					$b->an = 'Daeng Rosanda';
					break;
				case 'transfer_permata_1':
					$b->nama = 'BCA';
					$b->norek = '7750344563';
					$b->an = 'Daeng Rosanda';
					break;
				default:
					$b->nama = '-';
			}
			return $b;
		}
		public function __dateIndonesia($datetime,$utype='hari_tanggal'){
			$stt = strtotime($datetime);
			$bulan_ke = date('n',$stt);
			$bulan = 'Desember';
			switch ($bulan_ke) {
				case '1':
					$bulan = 'Januari';
					break;
				case '2':
					$bulan = 'Februari';
					break;
				case '3':
					$bulan = 'Maret';
					break;
				case '4':
					$bulan = 'April';
					break;
				case '5':
					$bulan = 'Mei';
					break;
				case '6':
					$bulan = 'Juni';
					break;
				case '7':
					$bulan = 'Juli';
					break;
				case '8':
					$bulan = 'Agustus';
					break;
				case '9':
					$bulan = 'September';
					break;
				case '10':
					$bulan = 'Oktober';
					break;
				case '11':
					$bulan = 'November';
					break;
				default:
					$bulan = 'Desember';
			}
			$hari_ke = date('N',$stt);
			$hari = 'Minggu';
			switch ($hari_ke) {
				case '1':
					$hari = 'Senin';
					break;
				case '2':
					$hari = 'Selasa';
					break;
				case '3':
					$hari = 'Rabu';
					break;
				case '4':
					$hari = 'Kamis';
					break;
				case '5':
					$hari = 'Jumat';
					break;
				case '6':
					$hari = 'Sabtu';
					break;
				default:
					$hari = 'Minggu';
			}
			$utype == strtolower($utype);
			if($utype=="hari") return $hari;
			if($utype=="jam") return date('H:i',$stt).' WIB';
			if($utype=="tanggal") return ''.date('d',$stt).' '.$bulan.' '.date('Y',$stt);
			if($utype=="tanggal_jam") return ''.date('d',$stt).' '.$bulan.' '.date('Y H:i',$stt).' WIB';
			if($utype=="hari_tanggal") return $hari.', '.date('d',$stt).' '.$bulan.' '.date('Y',$stt);
			if($utype=="hari_tanggal_jam") return $hari.', '.date('d',$stt).' '.$bulan.' '.date('Y H:i',$stt).' WIB';
		}
		public function __validateDate($date,$format="Y-m-d H:i:s"){
			$d = DateTime::createFromFormat($format, $date);
    	return $d && $d->format($format) == $date;
		}

		public function __getOrderStatusList(){
			return array(
				'order_konfirmasi'=>'Unconfirmed',
				'order_konfirmasi_sudah'=>'Confirmed',
				'order_cekstok'=>'In Check Stock',
				'order_pembelian'=>'Purchased',
				'order_store'=>'In Warehouse',
				'order_qc'=>'in Q.C.',
				'order_packing'=>'Packing',
				'order_kirim'=>'Sending',
				'order_selesai'=>'Done',
				'order_batal'=>'Canceled',
				'order_pending'=>'Hold on',
				'order_retur'=>'Return'
			);
		}
		public function __orderStatus($order_utype){
			switch($order_utype){
				case "order_konfirmasi_sudah":
					$os = 'Payment Verification';
					break;
				case "order_cekstok":
					$os = 'Stock Check (Process)';
					break;
				case "order_pembelian":
					$os = 'Purchasing (Process)';
					break;
				case "order_store":
					$os = 'Warehouse (Process)';
					break;
				case "order_qc":
					$os = 'QC (Process)';
					break;
				case "order_packing":
					$os = 'Packing (Process)';
					break;
				case "order_kirim":
					$os = 'Sent';
					break;
				case "order_selesai":
					$os = 'Finished';
					break;
				case "order_batal":
					$os = 'Batalkan';
					break;
				case "order_pending":
					$os = 'Pending';
					break;
				case "order_konfirmasi":
					$os = 'Unconfirmed';
					break;
				default:
					$os = 'Unknown';
			}
			return $os;
		}


		public function __orderPembayaran($order_pembayaran){
			switch($order_pembayaran){
				case "transfer_mandiri_1":
					$os = 'TF Mandiri 1';
					break;
				case "transfer_bni_1":
					$os = 'TF BNI 1';
					break;
				case "transfer_bca_1":
					$os = 'TF BCA 1';
					break;
				case "transfer_permata_1":
					$os = 'TF Permata 1';
					break;
				case "transfer_bri_1":
					$os = 'TF BRI 1';
					break;
				case "cash":
					$os = 'Tunai';
					break;
				default:
					$os = 'Tidak diketahui, hub admin!';
			}
			return $os;
		}

		public function __orderStatusLabel($order_utype){
			switch($order_utype){
				case "order_konfirmasi":
					$os = '<label class="label label-warning">Belum konfirmasi</label>';
					break;
				case "order_konfirmasi_sudah":
					$os = '<label class="label label-info">Verifikasi pembayaran</label>';
					break;
				case "order_cekstok":
					$os = '<label class="label label-info">Proses (Cekstok)</label>';
					break;
				case "order_pembelian":
					$os = '<label class="label label-info">Proses (Pembelian)</label>';
					break;
				case "order_store":
					$os = '<label class="label label-info">Proses (Gudang)</label>';
					break;
				case "order_qc":
					$os = '<label class="label label-info">Proses (QC)</label>';
					break;
				case "order_packing":
					$os = '<label class="label label-info">Proses (Packing)</label>';
					break;
				case "order_kirim":
					$os = '<label class="label label-info">Dikirim</label>';
					break;
				case "order_selesai":
					$os = '<label class="label label-info">Terkirim</label>';
					break;
				case "order_batal":
					$os = '<label class="label label-secondary">Dibatalkan</label>';
					break;
				case "order_pending":
					$os = '<label class="label label-info">Pending</label>';
					break;
				default:
					$os = '<label class="label label-info">Tidak diketahui, hub admin!</label></label>';
			}
			return $os;
		}
    public function __format($str,$format="text"){
      $format = strtolower($format);
      if($format == 'richtext'){
        $allowed_tags = '<div><h1><h2><h3><h4><u><hr><p><br><b><i><ul><ol><li><em><strong><quote><blockquote><p><time><sup><sub><table><tr><td><th><thead><tbody><tfoot>';
        return strip_tags($str,$allowed_tags);
      }else if($format == 'text'){
        return filter_var(trim($str), FILTER_SANITIZE_STRING);
      }else{
        return $str;
      }
    }
    public function __e($str,$format="text"){
      echo $this->__format($str,$format);
    }
    public function __f($str,$format="text"){
      return filter_var($str,FILTER_SANITIZE_SPECIAL_CHARS);
    }
    public function __g($str,$format="text"){
      return filter_var($str,FILTER_SANITIZE_STRING);
    }

		public function __pushNotif($tokens,$message,$title="Pemberitahuan"){
			if(!isset($this->fcm_server_token)){
				trigger_error('$this->fcm_server_token undefined!');
				die();
			}
			if(strlen($this->fcm_server_token)<=10){
				trigger_error('$this->fcm_server_token invalid!');
				die();
			}
			if(!is_array($tokens)){
				trigger_error('Token not array, aborted!');
				die();
			}
			$url = 'https://fcm.googleapis.com/fcm/send';

			$msg = array('body' => $message, 'title' => $title);
			$fields = array('registration_ids' => $tokens, 'notification' => $msg);
			$headers = array(
			'Authorization:key = '.$this->fcm_server_token,
			'Content-Type: application/json'
			);

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
			$result = curl_exec($ch);
			if ($result === FALSE) {
				die('Curl failed: ' . curl_error($ch));
			}
			curl_close($ch);
			//error_log('FCM FIRE: '.$result);
			$jres = json_decode($result);
			if(isset($jres->success)){
				return $jres;
			}else{
				$jres =  new stdClass();
				$jres->success = 0;
				$jres->failure = 1;
				return $jres;
			}
		}
		public function cdn_url($url=""){
			if($this->site_environment == 'local' || empty($this->site_environment)){
				return base_url($url);
			}
			if(strlen($this->cdn_url)>6){
				return $this->cdn_url.$url;
			}else{
				return base_url($url);
			}
		}

		//karena ini wajib jadi method ini harus ada... :P
		public function index(){}

}
