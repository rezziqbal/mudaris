<?php
class E_Soal_Pilihan_Model extends SENE_Model{
	var $tbl = 'e_soal_pilihan';
	var $tbl_as = 'esp';
	var $tbl2 = 'e_soal';
	var $tbl2_as = 'es';

	public function __construct(){
		parent::__construct();
		$this->db->from($this->tbl,$this->tbl_as);
	}

  public function setMass($esp_data){
    $this->db->insert_multi($this->tbl, $esp_data);
  }
	public function set($di){
		if(!is_array($di)) return 0;
		$this->db->insert($this->tbl,$di,0,0);
		return $this->db->last_id;
	}
	public function update($id,$du){
		if(!is_array($du)) return 0;
		$this->db->where("id",$id);
    return $this->db->update($this->tbl,$du,0);
	}
	public function del($id){
		$this->db->where("id",$id);
		return $this->db->delete($this->tbl);
	}
  public function getBySoalId($e_soal_id){
    $this->db->where("e_soal_id",$e_soal_id);
    return $this->db->get();
  }
  public function getByGuruIdUjianId($b_guru_id,$d_ujian_id){
    $this->db->select_as("$this->tbl_as.*, $this->tbl_as.id","id",0);
    $this->db->from($this->tbl,$this->tbl_as);
    $this->db->join($this->tbl2,$this->tbl2_as,'id',$this->tbl_as,'e_soal_id','');
    $this->db->where_as("$this->tbl2_as.b_guru_id",$b_guru_id);
    $this->db->where_as("$this->tbl2_as.d_ujian_id",$d_ujian_id);
    return $this->db->get();
  }
}
