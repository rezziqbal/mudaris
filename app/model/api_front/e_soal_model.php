<?php
class E_Soal_Model extends SENE_Model{
	var $tbl = 'e_soal';
	var $tbl_as = 'es';
	public function __construct(){
		parent::__construct();
		$this->db->from($this->tbl,$this->tbl_as);
	}

  public function trans_start(){
    $r = $this->db->autocommit(0);
    if($r) return $this->db->begin();
    return false;
  }

  public function trans_commit(){
    return $this->db->commit();
  }

  public function trans_rollback(){
    return $this->db->rollback();
  }

  public function trans_end(){
    return $this->db->autocommit(1);
  }

	public function set($di){
		if(!is_array($di)) return 0;
		$this->db->insert($this->tbl,$di,0,0);
		return $this->db->last_id;
	}
	public function update($id,$du){
		if(!is_array($du)) return 0;
		$this->db->where("id",$id);
    return $this->db->update($this->tbl,$du,0);
	}
	public function updateByIdSekolahId($a_sekolah_id,$id,$du){
		if(!is_array($du)) return 0;
		$this->db->where("a_sekolah_id",$a_sekolah_id);
		$this->db->where("id",$id);
    return $this->db->update($this->tbl,$du,0);
	}
	public function del($id){
		$this->db->where("id",$id);
		return $this->db->delete($this->tbl);
	}
	public function delByIdSekolahId($a_sekolah_id,$id){
		$this->db->where("a_sekolah_id",$a_sekolah_id);
		$this->db->where("id",$id);
		return $this->db->delete($this->tbl);
	}

  public function getByIdUjianId($id,$d_ujian_id){
    $this->db->where("d_ujian_id",$d_ujian_id);
    $this->db->where("id",$id);
    return $this->db->get_first();
  }
  public function getByGuruIdUjianId($b_guru_id,$d_ujian_id){
    $this->db->select();
    $this->db->from($this->tbl,$this->tbl_as);
    $this->db->where("b_guru_id",$b_guru_id);
    $this->db->where("d_ujian_id",$d_ujian_id);
		$this->db->order_by("id","desc");
    return $this->db->get();
  }
  public function countByGuruIdUjianId($b_guru_id,$d_ujian_id){
    $this->db->select_as("COUNT(*)","jumlah",0);
    $this->db->from($this->tbl,$this->tbl_as);
    $this->db->where("b_guru_id",$b_guru_id);
    $this->db->where("d_ujian_id",$d_ujian_id);
    $d = $this->db->get();
    if(isset($d->jumlah)) return $d->jumlah;
    return 0;
  }

  public function getByUjianId($d_ujian_id){
    $this->db->where("d_ujian_id",$d_ujian_id);
		$this->db->order_by("id","desc");
    return $this->db->get();
  }
}
