<?php
class D_Siswa_Model extends SENE_Model{
	var $tbl = 'd_siswa';
	var $tbl_as = 'ds';
	var $tbl2 = 'c_kelas';
	var $tbl2_as = 'ck';
	public function __construct(){
		parent::__construct();
		$this->db->from($this->tbl,$this->tbl_as);
	}
  public function getTableAlias(){
    return $this->tbl_as;
  }
  public function getTableAlias2(){
    return $this->tbl2_as;
  }
	public function getAll($page=0,$pagesize=10,$sortCol="kode",$sortDir="ASC",$keyword="",$sdate="",$edate=""){
		$this->db->flushQuery();
		$this->db->select();
		$this->db->from($this->tbl,$this->tbl_as);
		if(strlen($keyword)>0){
			$this->db->where("nama",$keyword,"OR","%like%",1,1);
		}
		$this->db->order_by($sortCol,$sortDir)->limit($page,$pagesize);
		return $this->db->get("object",0);
	}
	public function countAll($keyword="",$sdate="",$edate=""){
		$this->db->flushQuery();
		$this->db->select_as("COUNT(*)","jumlah",0);
		if(strlen($keyword)>0){
			$this->db->where("nama",$keyword,"OR","%like%",1,1);
		}
		$d = $this->db->from($this->tbl)->get_first("object",0);
		if(isset($d->jumlah)) return $d->jumlah;
		return 0;
	}
	public function getBySekolahId($a_sekolah_id,$page=0,$pagesize=10,$sortCol="kode",$sortDir="ASC",$keyword="",$sdate="",$edate=""){
		$this->db->flushQuery();
		$this->db->select_as("$this->tbl_as.id","id",0);
		$this->db->select_as("COALESCE($this->tbl2_as.nama,'-')","kelas",0);
		$this->db->select_as("$this->tbl_as.nourut","nourut",0);
		$this->db->select_as("$this->tbl_as.nama","nama",0);
		$this->db->select_as("$this->tbl_as.is_active","is_active",0);
		$this->db->from($this->tbl,$this->tbl_as);
		$this->db->join($this->tbl2,$this->tbl2_as,'id',$this->tbl_as,'c_kelas_id','left');
		$this->db->where_as("$this->tbl_as.a_sekolah_id",$a_sekolah_id,"AND","=");
		if(strlen($keyword)>0){
			$this->db->where_as("$this->tbl_as.nama",$keyword,"OR","%like%",1,0);
			$this->db->where_as("COALESCE($this->tbl2_as.nama,'-')",$keyword,"OR","%like%",0,1);
		}
		$this->db->order_by($sortCol,$sortDir)->limit($page,$pagesize);
		return $this->db->get("object",0);
	}
	public function countBySekolahId($a_sekolah_id,$keyword="",$sdate="",$edate=""){
		$this->db->flushQuery();
		$this->db->select_as("COUNT(*)","jumlah",0);
    $this->db->from($this->tbl,$this->tbl_as);
		$this->db->join($this->tbl2,$this->tbl2_as,'id',$this->tbl_as,'c_kelas_id','left');
		$this->db->where_as("$this->tbl_as.a_sekolah_id",$a_sekolah_id,"AND","=");
		if(strlen($keyword)>0){
			$this->db->where_as("$this->tbl_as.nama",$keyword,"OR","%like%",1,0);
			$this->db->where_as("COALESCE($this->tbl2_as.nama,'-')",$keyword,"OR","%like%",0,1);
		}
		$d = $this->db->get_first("object",0);
		if(isset($d->jumlah)) return $d->jumlah;
		return 0;
	}

	public function getBySekolahIdKelasId($a_sekolah_id,$c_kelas_id,$page=0,$pagesize=10,$sortCol="kode",$sortDir="ASC",$keyword="",$sdate="",$edate=""){
		$this->db->flushQuery();
		$this->db->select_as("$this->tbl_as.id","id",0);
		$this->db->select_as("$this->tbl_as.nourut","nourut",0);
		$this->db->select_as("$this->tbl_as.nama","nama",0);
		$this->db->select_as("$this->tbl_as.is_active","is_active",0);
		$this->db->from($this->tbl,$this->tbl_as);
		$this->db->where("a_sekolah_id",$a_sekolah_id,"AND","=");
		$this->db->where("c_kelas_id",$c_kelas_id,"AND","=");
		if(strlen($keyword)>0){
			$this->db->where("nama",$keyword,"OR","%like%",1,1);
		}
		$this->db->order_by($sortCol,$sortDir)->limit($page,$pagesize);
		return $this->db->get("object",0);
	}

	public function countBySekolahIdKelasId($a_sekolah_id,$c_kelas_id,$keyword="",$sdate="",$edate=""){
		$this->db->flushQuery();
		$this->db->select_as("COUNT(*)","jumlah",0);
    $this->db->from($this->tbl,$this->tbl_as);
		$this->db->where("a_sekolah_id",$a_sekolah_id,"AND","=");
		$this->db->where("c_kelas_id",$c_kelas_id,"AND","=");
		if(strlen($keyword)>0){
			$this->db->where("nama",$keyword,"OR","%like%",1,1);
		}
		$d = $this->db->get_first("object",0);
		if(isset($d->jumlah)) return $d->jumlah;
		return 0;
	}
	public function getById($id){
		$this->db->where("id",$id);
		return $this->db->get_first();
	}
	public function set($di){
		if(!is_array($di)) return 0;
		$this->db->insert($this->tbl,$di,0,0);
		return $this->db->last_id;
	}
	public function update($id,$du){
		if(!is_array($du)) return 0;
		$this->db->where("id",$id);
    return $this->db->update($this->tbl,$du,0);
	}
	public function updateByIdSekolahId($a_sekolah_id,$id,$du){
		if(!is_array($du)) return 0;
		$this->db->where("a_sekolah_id",$a_sekolah_id);
		$this->db->where("id",$id);
    return $this->db->update($this->tbl,$du,0);
	}
	public function del($id){
		$this->db->where("id",$id);
		return $this->db->delete($this->tbl);
	}
	public function delByIdSekolahId($a_sekolah_id,$id){
		$this->db->where("a_sekolah_id",$a_sekolah_id);
		$this->db->where("id",$id);
		return $this->db->delete($this->tbl);
	}
	public function checkKode($kode,$id=0){
		$this->db->select_as("COUNT(*)","jumlah",0);
		$this->db->where("kode",$kode);
		if(!empty($id)) $this->db->where("id",$id,'AND','!=');
		$d = $this->db->from($this->tbl)->get_first("object",0);
		if(isset($d->jumlah)) return $d->jumlah;
		return 0;
	}
	public function getByIdDanSekolahId($id,$a_sekolah_id){
		$this->db->select_as("$this->tbl_as.*, COALESCE($this->tbl_as.c_kelas_id,'null')",'c_kelas_id',0);
		$this->db->from($this->tbl,$this->tbl_as);
    $this->db->where("id",$id)->where("a_sekolah_id",$a_sekolah_id);
		return $this->db->get_first("object",0);
	}
  public function getNoUrut($a_sekolah_id,$c_kelas_id){
    $this->db->select_as("COALESCE(nourut,1)+1",'nourut',0);
    $this->db->from($this->tbl,$this->tbl_as);
    $this->db->where("c_kelas_id",$c_kelas_id);
    $this->db->where("a_sekolah_id",$a_sekolah_id);
    $this->db->order_by("id","desc");
		$d = $this->db->get_first("object",0);
    if(isset($d->nourut)) return (int) $d->nourut;
    return 1;
  }
}
