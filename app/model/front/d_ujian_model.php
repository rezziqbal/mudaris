<?php
class D_Ujian_Model extends SENE_Model{
	var $tbl = 'd_ujian';
	var $tbl_as = 'ds';
	var $tbl2 = 'c_matapelajaran';
	var $tbl2_as = 'cmp';
	public function __construct(){
		parent::__construct();
		$this->db->from($this->tbl,$this->tbl_as);
	}
  public function getTableAlias(){
    return $this->tbl_as;
  }
  public function getTableAlias2(){
    return $this->tbl2_as;
  }
	public function getByIdSekolahIdGuruId($a_sekolah_id,$b_guru_id,$id){
		$this->db->select_as("$this->tbl_as.*, COALESCE($this->tbl_as.c_matapelajaran_id,'null')",'c_matapelajaran_id',0);
		$this->db->from($this->tbl,$this->tbl_as);
    $this->db->where("a_sekolah_id",$a_sekolah_id)->where("b_guru_id",$b_guru_id)->where("id",$id);
		return $this->db->get_first("object",0);
	}
	public function getByIdGuruId($b_guru_id,$id){
		$this->db->select_as("$this->tbl_as.*, COALESCE($this->tbl_as.c_matapelajaran_id,'null')",'c_matapelajaran_id',0);
		$this->db->from($this->tbl,$this->tbl_as);
    $this->db->where("b_guru_id",$b_guru_id)->where("id",$id);
		return $this->db->get_first("object",0);
	}
}
