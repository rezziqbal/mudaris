<?php
class C_MataPelajaran_Model extends SENE_Model{
	var $tbl = 'c_matapelajaran';
	var $tbl_as = 'cmp';
	public function __construct(){
		parent::__construct();
		$this->db->from($this->tbl,$this->tbl_as);
	}

	public function trans_start(){
		$r = $this->db->autocommit(0);
		if($r) return $this->db->begin();
		return false;
	}

	public function trans_commit(){
		return $this->db->commit();
	}

	public function trans_rollback(){
		return $this->db->rollback();
	}

	public function trans_end(){
		return $this->db->autocommit(1);
	}

  public function set($di){
    $this->db->insert($this->tbl,$di);
    return $this->db->last_id;
  }
  public function update($id,$du){
    $this->db->where("id",$id);
    return $this->db->update($this->tbl,$du);
  }
  public function delete($id){
    $this->db->where("id",$id);
    return $this->db->delete($this->tbl);
  }
  public function check($email){
    $this->db->where_as("$this->tbl_as.email",$this->db->esc($email));
    return $this->db->get_first('',0);
  }
  public function getById($id){
    $this->db->where_as("$this->tbl_as.id",$this->db->esc($id));
    return $this->db->get_first();
  }
  public function getMataPelajaranSaya($a_sekolah_id, $b_guru_id){
    $this->db->select("id")->select("nama");
    $this->db->from($this->tbl,$this->tbl_as);
    $this->db->where("a_sekolah_id",$a_sekolah_id)->where("b_guru_id",$b_guru_id);
    $this->db->order_by("nama","ASC");
    return $this->db->get();
  }
}
