<?php
class A_Sekolah_Model extends SENE_Model{
	var $tbl = 'a_sekolah';
	var $tbl_as = 'ase';
	public function __construct(){
		parent::__construct();
		$this->db->from($this->tbl,$this->tbl_as);
	}
  public function set($di){
    $this->db->insert($this->tbl,$di);
    return $this->db->last_id;
  }
  public function update($id,$du){
    $this->db->where("id",$id);
    return $this->db->update($this->tbl,$du);
  }
  public function delete($id){
    $this->db->where("id",$id);
    return $this->db->delete($this->tbl);
  }
  public function check($nama){
    $this->db->where_as("$this->tbl_as.nama",$this->db->esc($nama));
    return $this->db->get_first();
  }
	public function getById($id){
		$this->db->from($this->tbl,$this->tbl_as);
    $this->db->where_as("$this->tbl_as.id",$this->db->esc($id));
    return $this->db->get_first();
	}
}
