<?php
class B_Menu_Model extends SENE_Model{
	var $tbl = 'b_menu';
	var $tbl_as = 'bmn';
	public function __construct(){
		parent::__construct();
		$this->db->from($this->tbl,$this->tbl_as);
	}
  public function getActive(){
    $this->db->select_as("*, COALESCE(b_menu_id)",'b_menu_id',0);
    $this->db->where("is_active",1);
    $this->db->order_by("priority",'asc');
    return $this->db->get();
  }
}
