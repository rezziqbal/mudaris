<?php
class B_Guru_Model extends SENE_Model{
	var $tbl = 'b_guru';
	var $tbl_as = 'bgr';
	public function __construct(){
		parent::__construct();
		$this->db->from($this->tbl,$this->tbl_as);
	}

	public function trans_start(){
		$r = $this->db->autocommit(0);
		if($r) return $this->db->begin();
		return false;
	}

	public function trans_commit(){
		return $this->db->commit();
	}

	public function trans_rollback(){
		return $this->db->rollback();
	}

	public function trans_end(){
		return $this->db->autocommit(1);
	}

  public function set($di){
    $this->db->insert($this->tbl,$di);
    return $this->db->last_id;
  }
  public function update($id,$du){
    $this->db->where("id",$id);
    return $this->db->update($this->tbl,$du);
  }
  public function delete($id){
    $this->db->where("id",$id);
    return $this->db->delete($this->tbl);
  }
  public function check($email){
    $this->db->where("email",$email);
    return $this->db->get_first('',0);
  }
  public function getById($id){
    $this->db->where("id",$id);
    return $this->db->get_first();
  }
}
