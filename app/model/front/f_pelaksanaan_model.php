<?php
class F_Pelaksanaan_Model extends SENE_Model{
	var $tbl = 'f_pelaksanaan';
	var $tbl_as = 'fp';
	var $tbl2 = 'c_kelas';
	var $tbl2_as = 'ck';
	var $tbl3 = 'd_ujian';
	var $tbl3_as = 'du';
	var $tbl4 = 'c_matapelajaran';
	var $tbl4_as = 'cmp';

	public function __construct(){
		parent::__construct();
		$this->db->from($this->tbl,$this->tbl_as);
	}
  public function getTableAlias(){
    return $this->tbl_as;
  }
  public function getTableAlias2(){
    return $this->tbl2_as;
  }
  public function getTableAlias3(){
    return $this->tbl3_as;
  }
  public function getTableAlias4(){
    return $this->tbl4_as;
  }
  public function getByIdGuruId($b_guru_id,$id){
    $this->db->where("b_guru_id",$b_guru_id)->where("id",$id);
    return $this->db->get_first();
  }
}
