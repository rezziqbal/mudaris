<?php
class A_Sekolah_Model extends SENE_Model{
	var $tbl = 'a_sekolah';
	var $tbl_as = 'ase';
	public function __construct(){
		parent::__construct();
		$this->db->from($this->tbl,$this->tbl_as);
	}
  public function getById($id){
    $this->db->where_as("$this->tbl_as.id",$this->db->esc($id));
    $this->db->order_by("nama","asc");
    return $this->db->get_first();
  }
  public function search($nama){
    if(strlen($nama)) $this->db->where_as("$this->tbl_as.nama",$this->db->esc($nama),'AND','%like%');
    $this->db->order_by("nama","asc");
    return $this->db->get();
  }
}
