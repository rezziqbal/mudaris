<?php
class D_Siswa_Model extends SENE_Model{
	var $tbl = 'd_siswa';
	var $tbl_as = 'dsw';
	var $tbl2 = 'c_kelas';
	var $tbl2_as = 'ck';
	var $tbl3 = 'a_sekolah';
	var $tbl3_as = 'ash';
	public function __construct(){
		parent::__construct();
		$this->db->from($this->tbl,$this->tbl_as);
	}
  public function set($di){
    $this->db->insert($this->tbl,$di);
    return $this->db->last_id;
  }

	public function update($a_sekolah_id, $c_kelas_id, $id, $du){
		if(!is_array($du)) return 0;
		$this->db->where("a_sekolah_id",$a_sekolah_id);
		$this->db->where("c_kelas_id",$c_kelas_id);
		$this->db->where("id",$id);
    return $this->db->update($this->tbl,$du,0);
	}

  public function check($a_sekolah_id,$c_kelas_id,$nama){
    $this->db->flushQuery();
    $this->db->select_as("$this->tbl_as.*, COALESCE($this->tbl_as.api_mobile_token,'-')",'api_mobile_token',0);
    $this->db->select_as("COALESCE($this->tbl2_as.nama,'-')",'kelas',0);
    $this->db->select_as("COALESCE($this->tbl3_as.nama,'-')",'sekolah',0);

		$this->db->from($this->tbl,$this->tbl_as);
    $this->db->join($this->tbl2,$this->tbl2_as,"id",$this->tbl_as,"c_kelas_id","left");
    $this->db->join($this->tbl3,$this->tbl3_as,"id",$this->tbl_as,"a_sekolah_id","left");
    $this->db->where_as("$this->tbl_as.a_sekolah_id",$a_sekolah_id);
    $this->db->where_as("$this->tbl_as.c_kelas_id",$c_kelas_id);
    $this->db->where_as("$this->tbl_as.nama",$this->db->esc($nama));
    $this->db->order_by("$this->tbl_as.id","asc");
    return $this->db->get_first();
  }

  public function getById($id){
    $this->db->flushQuery();
    $this->db->select_as("$this->tbl_as.*, COALESCE($this->tbl_as.api_mobile_token,'-')",'api_mobile_token',0);
    $this->db->select_as("COALESCE($this->tbl2_as.nama,'-')",'kelas',0);
    $this->db->select_as("COALESCE($this->tbl3_as.nama,'-')",'sekolah',0);

		$this->db->from($this->tbl,$this->tbl_as);
    $this->db->join($this->tbl2,$this->tbl2_as,"id",$this->tbl_as,"c_kelas_id","left");
    $this->db->join($this->tbl3,$this->tbl3_as,"id",$this->tbl_as,"a_sekolah_id","left");
    $this->db->where_as("$this->tbl_as.id",$this->db->esc($id));
    $this->db->order_by("$this->tbl_as.id","asc");
    return $this->db->get_first();
  }
  public function getByToken($token,$kind="api_mobile_token"){
    $this->db->where_as("$this->tbl_as.$kind",$this->db->esc($token));
    $this->db->order_by("nama","asc");
    return $this->db->get_first();
  }
}
