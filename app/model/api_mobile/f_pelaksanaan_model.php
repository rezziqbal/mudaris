<?php
class F_Pelaksanaan_Model extends SENE_Model{
	var $tbl = 'f_pelaksanaan';
	var $tbl_as = 'fpm';
	var $tbl2 = 'd_ujian';
	var $tbl2_as = 'du';

	public function __construct(){
		parent::__construct();
		$this->db->from($this->tbl,$this->tbl_as);
	}
  public function getByKelasIdMataPelajaranId($c_kelas_id,$c_matapelajaran_id){
		$this->db->from($this->tbl,$this->tbl_as);
		$this->db->join($this->tbl2,$this->tbl2_as,'id',$this->tbl_as,'d_ujian_id');
    $this->db->where_as("c_kelas_id",$this->db->esc($c_kelas_id));
    $this->db->where_as("$this->tbl2_as.c_matapelajaran_id",$this->db->esc($c_matapelajaran_id));
		$this->db->where_in("$this->tbl_as.pelaksanaan_status",array('dimulai','sedang_berlangsung'));
    return $this->db->get_first();
  }
}
