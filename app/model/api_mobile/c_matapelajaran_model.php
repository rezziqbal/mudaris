<?php
class C_Matapelajaran_Model extends SENE_Model{
	var $tbl = 'c_matapelajaran';
	var $tbl_as = 'cmp';
	var $tbl2 = 'c_kelas';
	var $tbl2_as = 'ck';
	var $tbl3 = 'a_sekolah';
	var $tbl3_as = 'ash';
	var $tbl4 = 'b_guru';
	var $tbl4_as = 'bgr';

	public function __construct(){
		parent::__construct();
		$this->db->from($this->tbl,$this->tbl_as);
	}
  public function getByKelasId($a_sekolah_id,$c_kelas_id){
    $this->db->flushQuery();
		$this->db->select_as("$this->tbl_as.*, $this->tbl_as.id","id",0);
		$this->db->select_as("COALESCE($this->tbl4_as.nama,'-')","guru",0);
		$this->db->from($this->tbl,$this->tbl_as);
		$this->db->join($this->tbl4,$this->tbl4_as,'id',$this->tbl_as,'b_guru_id','left');
    $this->db->where_as("$this->tbl_as.a_sekolah_id",$a_sekolah_id);
    $this->db->where_as("$this->tbl_as.c_kelas_id",$c_kelas_id);
    $this->db->order_by("$this->tbl_as.id","asc");
    return $this->db->get();
  }
  public function getById($a_sekolah_id,$c_kelas_id,$id){
    $this->db->flushQuery();
		$this->db->select_as("$this->tbl_as.*, $this->tbl_as.id","id",0);
		$this->db->select_as("COALESCE($this->tbl4_as.nama,'-')","guru",0);
		$this->db->from($this->tbl,$this->tbl_as);
		$this->db->join($this->tbl4,$this->tbl4_as,'id',$this->tbl_as,'b_guru_id','left');
    $this->db->where_as("$this->tbl_as.id",$id);
    $this->db->where_as("$this->tbl_as.a_sekolah_id",$a_sekolah_id);
    $this->db->where_as("$this->tbl_as.c_kelas_id",$c_kelas_id);
    $this->db->order_by("$this->tbl_as.id","asc");
    return $this->db->get_first();
  }

  public function getBySekolahId($a_sekolah_id){
    $this->db->where("a_sekolah_id",$a_sekolah_id);
    $this->db->where("is_active",1);
    $this->db->order_by("nama","ASC");
    return $this->db->get();
  }
}
