<?php
class E_Soal_Pilihan_Model extends SENE_Model{
	var $tbl = 'e_soal_pilihan';
	var $tbl_as = 'eslp';
	var $tbl2 = 'e_soal';
	var $tbl2_as = 'esl';
	public function __construct(){
		parent::__construct();
		$this->db->from($this->tbl,$this->tbl_as);
	}
  public function getByUjianID($d_ujian_id){
    $this->db->select_as("$this->tbl_as.*, $this->tbl_as.id","id",0);
    $this->db->from($this->tbl,$this->tbl_as);
    $this->db->join($this->tbl2,$this->tbl2_as,"id",$this->tbl_as,"e_soal_id",'left');
    $this->db->where_as("$this->tbl2_as.d_ujian_id",$d_ujian_id);
    return $this->db->get();
  }
}
