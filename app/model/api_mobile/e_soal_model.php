<?php
class E_Soal_Model extends SENE_Model{
	var $tbl = 'e_soal';
	var $tbl_as = 'esl';
	public function __construct(){
		parent::__construct();
		$this->db->from($this->tbl,$this->tbl_as);
	}
  public function getByUjianID($d_ujian_id){
    $this->db->from($this->tbl,$this->tbl_as);
    $this->db->where("d_ujian_id",$d_ujian_id);
    return $this->db->get();
  }
}
