<?php
class B_Guru_Model extends SENE_Model{
	var $tbl = 'b_guru';
	var $tbl_as = 'bgr';
	public function __construct(){
		parent::__construct();
		$this->db->from($this->tbl,$this->tbl_as);
	}
  public function search($nama){
    if(strlen($nama)) $this->db->where_as("$this->tbl_as.nama",$this->db->esc($nama),'AND','%like%');
    $this->db->order_by("nama","asc");
    return $this->db->get();
  }
	public function getById($a_sekolah_id, $id){
		$this->db->where("a_sekolah_id",$a_sekolah_id);
		$this->db->where("id",$id);
		return $this->db->get_first();
	}
}
