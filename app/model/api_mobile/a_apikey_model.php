<?php
class A_ApiKey_Model extends SENE_Model{
	var $tbl = 'a_apikey';
	var $tbl_as = 'aak';
	public function __construct(){
		parent::__construct();
		$this->db->from($this->tbl,$this->tbl_as);
	}
	public function getAlias(){
		return $this->tbl_as;
	}
  public function get(){
    //$this->db->select_as("$this->tbl_as.*, AES_ENCRYPT(`code`,UNHEX('A2B1C3D4'))",'code',0);
		$this->db->from($this->tbl,$this->tbl_as);
    $this->db->where("is_active",1);
    return $this->db->get('',0);
  }
  public function getByCode($code){
    $this->db->where("code",$code);
    $this->db->where("is_active",1);
    return $this->db->get_first();
  }
  public function countByCode($code){
    $this->db->select_as("COUNT(*)",'total',0);
    $this->db->where("code",$code);
    $this->db->where("is_active",1);
    $d = $this->db->get_first();
    if(isset($d->total)) return (int) $d->total;
    return 0;
  }
}
