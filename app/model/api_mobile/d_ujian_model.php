<?php
class D_Ujian_Model extends SENE_Model{
	var $tbl = 'd_ujian';
	var $tbl_as = 'dujn';
	var $tbl2 = 'd_matapelajaran';
	var $tbl2_as = 'dmp';

	public function __construct(){
		parent::__construct();
		$this->db->from($this->tbl,$this->tbl_as);
	}

  public function getById($a_sekolah_id,$c_kelas_id,$d_matapelajaran_id){
    $this->db->flushQuery();
		$this->db->select_as("$this->tbl_as.*, $this->tbl_as.id","id",0);
		$this->db->select_as("COALESCE($this->tbl2_as.nama,'-')","matapelajaran",0);
		$this->db->from($this->tbl,$this->tbl_as);
		$this->db->join($this->tbl2,$this->tbl2_as,'id',$this->tbl_as,'d_matapelajaran_id','left');
    $this->db->where_as("$this->tbl_as.d_matapelajaran_id",$d_matapelajaran_id);
    $this->db->where_as("$this->tbl_as.a_sekolah_id",$a_sekolah_id);
    $this->db->where_as("$this->tbl_as.c_kelas_id",$c_kelas_id);
    $this->db->order_by("$this->tbl_as.id","asc");
    return $this->db->get_first();
  }
}
