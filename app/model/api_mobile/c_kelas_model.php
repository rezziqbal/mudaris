<?php
class C_Kelas_Model extends SENE_Model{
	var $tbl = 'c_kelas';
	var $tbl_as = 'cks';
	public function __construct(){
		parent::__construct();
		$this->db->from($this->tbl,$this->tbl_as);
	}
  public function search($nama){
    if(strlen($nama)) $this->db->where_as("$this->tbl_as.nama",$this->db->esc($nama),'AND','%like%');
    $this->db->order_by("nama","asc");
    return $this->db->get();
  }
  public function getBySekolahId($a_sekolah_id){
    $this->db->where_as("$this->tbl_as.a_sekolah_id",$a_sekolah_id);
    $this->db->order_by("nama","ASC");
    return $this->db->get();
  }

  public function getByIdSekolahId($id,$a_sekolah_id){
    $this->db->where_as("$this->tbl_as.id",$id);
    $this->db->where_as("$this->tbl_as.a_sekolah_id",$a_sekolah_id);
    $this->db->order_by("nama","ASC");
    return $this->db->get_first();
  }
}
