<!-- modal kelas -->
<div id="kelas_pilih_modal" class="modal fade " role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header text-center">
				<h2 class="modal-title">Pilih Kelas</h2>
			</div>
			<!-- END Modal Header -->

			<!-- Modal Body -->
			<div class="modal-body">
        <form id="pilih_kelas_form" action="" method="post">
  				<div class="row">
  					<div class="col-xs-12 btn-group-vertical">
              <select id="" class="form-control">
                <?php if(isset($kelas)){ foreach($kelas as $kl){ ?>
                <option value="<?=$kl->id?>"><?=$kl->nama?></option>
                <?php } } ?>
              </select>
  					</div>
  				</div>
  				<div class="row" style="margin-top: 1em; ">
  					<div class="col-md-12" style="border-top: 1px #afafaf dashed;">&nbsp;</div>
  					<div class="col-xs-12 btn-group-vertical" style="">
  						<button type="submit" class="btn btn-primary btn-block" style="text-align: left;"><i class="fa fa-save"></i> Simpan</button>
              <button type="button" class="btn btn-default btn-block" style="text-align: left;" data-dismiss="modal"><i class="fa fa-times"></i> Tutup</button>
            </div>
  				</div>
        </form>
			</div> <!-- END Modal Body -->
		</div>
	</div>
</div>
<!-- modal pilihan -->
