<div class="row halaman-judul-wrapper">
  <div class="col-md-8 halaman-judul">
    <h1>Pelaksanaan Ujian</h1>
  </div>
  <div class="col-md-4 halaman-aksi">
    <div class="btn-group pull-right">
      <a id="ahentikan" href="#" class="btn btn-danger"><i class="fa fa-stop"></i> Selesai</a>
      <a id="ajeda" href="#" class="btn btn-warning"><i class="fa fa-pause"></i> Jeda</a>
      <a id="amulai" href="#" class="btn btn-success"><i class="fa fa-play"></i> Mulai</a>
    </div>
  </div>
</div>
<div class="row halaman-judul-wrapper">
  <div class="col-md-5">
    <table class="table ">
      <tr>
        <td class="col-md-3">Nama Ujian</td>
        <td>:</td>
        <td><?=$ujian->nama?></td>
      </tr>
      <tr>
        <td>Mata Pelajaran</td>
        <td>:</td>
        <td><?=$matapelajaran[$ujian->c_matapelajaran_id]->nama?></td>
      </tr>
      <?php if($ujian->sifat != 'hide'){ ?>
      <tr>
        <td class="col-md-3">Sifat</td>
        <td>:</td>
        <td><?=$this->__sifat($ujian->sifat)?></td>
      </tr>
      <?php } ?>
      <tr>
        <td>Kelas</td>
        <td>:</td>
        <td id="td_kelas"><?=$pelaksanaan_kelas->nama?></td>
      </tr>
    </table>
  </div>
  <div class="col-md-2">&nbsp;</div>
  <div class="col-md-5">
    <table class="table ">
      <tr>
        <td class="col-md-3">Tgl Mulai</td>
        <td>:</td>
        <td><?=$this->__dateIndonesia($pelaksanaan->sdate,"hari_tanggal_jam")?></td>
      </tr>
      <tr>
        <td class="col-md-3">Tgl Selesai</td>
        <td>:</td>
        <td><?=$this->__dateIndonesia($pelaksanaan->edate,"hari_tanggal_jam")?></td>
      </tr>
      <tr>
        <td class="col-md-3">Status</td>
        <td>:</td>
        <td id="td_pelaksanaan_status"><?=$pelaksanaan->pelaksanaan_status?></td>
      </tr>
    </table>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <p>
      Daftar siswa yang mengikuti ujian:
    </p>
    <div class="table-reponsive">
      <table id="drTable" class="table table-bordered">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nama Siswa</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td colspan="3"><div class="alert alert-info">Belum ada siswa yang bergabung</div></td>
          </tr>
        </tbody>
      </table>
    </div>

  </div>
</div>
