var ieid = '';
var drTable = {};

function gritter(teks,jenis="info"){
  $.bootstrapGrowl(teks, {
    type: jenis,
    delay: 2500,
    allow_dismiss: true
  });
};

$('.datepicker').datepicker({
  format: "yyyy-mm-dd",
  startDate: new Date(),
});

$('.clockpicker').clockpicker({
  autoclose: true
});

//date default
$("#isdate").on("change",function(e){
  $("#iedate").val($(this).val());
});

if(jQuery('#drTable').length>0){
	drTable = jQuery('#drTable')
	.on('preXhr.dt', function ( e, settings, data ){
    NProgress.start();
	}).DataTable({
			"order"					: [[ 1, "asc" ]],
			"responsive"	  : true,
			"bProcessing"		: true,
			"bServerSide"		: true,
			"sAjaxSource"		: "<?=base_url("api_front/pelaksanaan/"); ?>",
			"fnServerData"	: function (sSource, aoData, fnCallback, oSettings) {
				oSettings.jqXHR = $.ajax({
					dataType 	: 'json',
					method 		: 'POST',
					url 		: sSource,
					data 		: aoData
				}).success(function (response, status, headers, config) {
					console.log(response);
          NProgress.done();
					$('#drTable > tbody').off('click', 'tr');
					$('#drTable > tbody').on('click', 'tr', function (e) {
						e.preventDefault();
						var id = $(this).find("td").html();
						ieid = id;
						var url = '<?=base_url("api_front/pelaksanaan/detail/")?>'+encodeURIComponent(ieid);
						$.get(url).done(function(response){
							if(response.status==200){
								var dta = response.data;
								//input nilai awal
                $("#ienama").val(dta.nama);
                $("#iec_matapelajaran_id").val(dta.c_matapelajaran_id);
                $("#iesifat").val(dta.sifat);
                $("#iejml_opsi").val(dta.jml_opsi);
                $("#ienourut").val(dta.nourut);
                $("#ieis_active").val(dta.is_active);

                //input url
                $("#adetail").attr("href",'<?=base_url("pelaksanaan/detail/")?>'+encodeURIComponent(ieid));

								//tampilkan modal
								$("#pilihan_modal").modal("show");
							}else{
								gritter('<h4>Galat</h4><p>'+response.message+'</p>','danger');
							}

						});
					});
					fnCallback(response);
				}).fail(function (response, status, headers, config) {
          NProgress.done();
					gritter('<h4>Galat</h4><p>Tidak dapat mengambil data</p>','warning');
				});
			},
	});
	$('.dataTables_filter input').attr('placeholder', 'Cari');
}

$("#atambah").on("click",function(e){
  e.preventDefault();
  $("#tambah_modal_form").trigger("reset");
  $("#tambah_modal").modal("show");
});
$("#tambah_modal_form").on("submit",function(e){
  e.preventDefault();
  NProgress.start();
  var fd = new FormData($(this)[0]);
  $.ajax({
    url: '<?=base_url('api_front/pelaksanaan/tambah/'); ?>',
		type: "POST",
		data: fd,
		contentType: false,
		cache: false,
		processData:false,
		success: function(data){
			NProgress.done();
      $("#tambah_modal").modal("hide");
			if(data.status == 200){
        gritter('<h4>Berhasil</h4><p>Data baru berhasil ditambahkan</p>','success');
				drTable.ajax.reload(null,false);
			}else{
				gritter('<h4>Gagal</h4><p>'+data.message+'</p>','danger');
			}
		},
		error: function(data){
			NProgress.done();
			gritter('<h4>Error</h4><p>Saat ini proses penambahan data sedang error, coba lagi nanti</p>','warning');
		}
  });
});

$("#aedit").on("click",function(e){
  NProgress.start();
  $("#pilihan_modal").modal("hide");
  NProgress.set(0.7);
  setTimeout(function(){
    NProgress.done();
    $("#edit_modal").modal("show");
  },666);
});
$("#edit_modal_form").on("submit",function(e){
  e.preventDefault();
  NProgress.start();
  var fd = new FormData($(this)[0]);
  $.ajax({
    url: '<?=base_url('api_front/pelaksanaan/edit/'); ?>'+encodeURIComponent(ieid),
		type: "POST",
		data: fd,
		contentType: false,
		cache: false,
		processData:false,
		success: function(data){
			NProgress.done();
      $("#edit_modal").modal("hide");
			if(data.status == 200){
        gritter('<h4>Berhasil</h4><p>Data berhasil diubah</p>','success');
				drTable.ajax.reload(null,false);
			}else{
				gritter('<h4>Gagal</h4><p>'+data.message+'</p>','danger');
			}
		},
		error: function(data){
			NProgress.done();
			gritter('<h4>Error</h4><p>Saat ini proses perubahan data sedang error, coba lagi nanti</p>','warning');
		}
  });
});

$("#bhapus").on("click",function(e){
  e.preventDefault();
  if(ieid){
    var c = confirm('Apakah anda yakin?');
    if(c){
			NProgress.start();
      $.get("<?=base_url("api_front/pelaksanaan/hapus/")?>"+encodeURIComponent(ieid)).done(function(dt){
  			NProgress.done();
        $("#pilihan_modal").modal("hide");
        if(dt.status==100){
          gritter('<h4>Berhasil</h4><p>Data berhasil dihapus</p>','success');
          drTable.ajax.reload(null,false);
        }else{
          gritter('<h4>Gagal</h4><p>'+data.message+'</p>','danger');
        }
      }).fail(function(){
        NProgress.done();
  			gritter('<h4>Error</h4><p>Saat ini proses penghapusan data sedang error, coba lagi nanti</p>','warning');
  		});
    }
  }
});


$("#ic_matapelajaran_id").on("change",function(e){
  e.preventDefault();
  NProgress.start();
  var cmpid = $(this).val();
  $("#id_ujian_id").html('<option value="null">--Pilih Ujian--</option>');
  $.get('<?=base_url("api_front/ujian/mata_pelajaran/")?>'+encodeURIComponent(cmpid)).done(function(dt){
    NProgress.done();
    if(dt.status == 200){
      $.each(dt.data,function(k,v){
        $("#id_ujian_id").append('<option value="'+v.id+'">'+v.nama+'</option>');
      });
    }else{
      gritter("<h4>Gagal</h4><p>"+dt.message+"</p>",'danger');
    }
  }).fail(function(){
    NProgress.done();
    gritter("<h4>Error</h4><p>Tidak dapat mengambil data ujian dari server, coba lagi nanti</p>",'warning');
  })
})
