var ieid = '';
var e_ujian_id = '<?=$ujian->id?>';
var soal_method = 'tambah';

function gritter(teks,jenis="info"){
  $.bootstrapGrowl(teks, {
    type: jenis,
    delay: 2500,
    allow_dismiss: true
  });
};

$("#a_pilih_kelas").on("click",function(e){
  e.preventDefault();
  $("#kelas_pilih_modal").modal("show");
});

$("#a_pilih_kelas").trigger("click");

$("#pilih_kelas_form").on("submit",function(e){
  e.preventDefault();
  NProgress.start();
  var fd = new FormData($(this)[0]);
  $.ajax({
    url: '<?=base_url('api_front/pelaksanaan/pilihkelas/'); ?>',
		type: "POST",
		data: fd,
		contentType: false,
		cache: false,
		processData:false,
		success: function(data){
			NProgress.done();
      $("#tambah_modal").modal("hide");
			if(data.status == 200){
        gritter('<h4>Berhasil</h4><p>Data baru berhasil ditambahkan</p>','success');
				drTable.ajax.reload(null,false);
			}else{
				gritter('<h4>Gagal</h4><p>'+data.message+'</p>','danger');
			}
		},
		error: function(data){
			NProgress.done();
			gritter('<h4>Error</h4><p>Saat ini proses penambahan data sedang error, coba lagi nanti</p>','warning');
		}
  });
});
