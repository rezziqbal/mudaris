<div class="top-nav">
  <div class="container">
    <div class="row col-md-12">
      <div class="top-nav-text"></div>
      <div class="top-nav-icon-blocks">
        <?php if(strlen($this->site_social_email)>4){?>
        <div class="icon-block">
          <p>
            <a href="mailto:<?=$this->site_social_email?>" target="_blank"><i class="fa fa-envelope-open-o"></i><span></span></a>
          </p>
        </div>
        <?php } ?>
        <?php if(strlen($this->site_social_ig)>4){?>
        <div class="icon-block">
          <p>
            <a href="<?=$this->site_social_ig?>" target="_blank"><i class="fa fa-instagram"></i><span></span></a>
          </p>
        </div>
        <?php } ?>
        <?php if(strlen($this->site_social_fb)>4){?>
        <div class="icon-block">
          <p><a href="<?=$this->site_social_fb?>" target="_blank"><i class="fa fa-facebook"></i><span></span></a></p>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
