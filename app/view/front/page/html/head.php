<head>
  <!-- Basic page needs -->
  <meta charset="utf-8">
  <!-- Mobile specific metas  -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <![endif]-->
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title><?=$this->getTitle(); ?></title>
  <meta name="language" content="id" />
  <meta name="description" content="<?=$this->getDescription(); ?>"/>
  <meta name="keyword" content="<?=$this->getKeyword(); ?>"/>
  <meta name="author" content="<?=$this->getAuthor(); ?>">
  <link rel="icon" href="<?=$this->getIcon(); ?>" type="image/x-icon" />
  <link rel="shortcut icon" href="<?=$this->getShortcutIcon(); ?>" type="image/x-icon" />
  <meta name="robots" content="<?=$this->getRobots(); ?>" />

  <!-- prefetch -->
  <meta http-equiv="x-dns-prefetch-control" content="on">
  <link rel="dns-prefetch" href="//www.googletagmanager.com" />
  <link rel="dns-prefetch" href="//connect.facebook.net" />
  <link rel="dns-prefetch" href="//www.facebook.com" />
  <link rel="dns-prefetch" href="//fonts.googleapis.com" />

  <!-- geo -->
  <meta name="geo.region" content="" />
  <meta name="geo.placename" content="" />
  <meta name="geo.position" content="" />
  <meta name="ICBM" content="" />


  <!-- other meta -->
  <meta property="fb:app_id" content="" />
  <meta property="og:image" content="" />
  <link href="https://fonts.googleapis.com/css?family=Eczar|Lato" rel="stylesheet" />

  <?php $this->getAdditionalBefore(); ?>
  <?php $this->getAdditional(); ?>
  <?php $this->getAdditionalAfter(); ?>

  <!-- google meta tag -->
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-61723221-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-61723221-1');
</script>

  <!-- end google meta tag-->
  <!--facebook analytics-->
  <!--end facebook analytics-->
</head>
