<div class="footer-menu">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="logo-container">
          <img src="<?=base_url("skin/front/images/logo-text.png");?>" class="img-responsive" style="height: 100px;" />
        </div>
      </div>
      <div class="col-md-4">
        <ul class="nav nav-pills nav-stacked">
          <?php if(isset($menu_footer_1)){ foreach($menu_footer_1 as $m1){ ?>
            <?php $url = $m1->url; if($m1->url_type == 'internal') $url = base_url($m1->url); ?>
            <?php if($m1->url == '#') $url = '#'; ?>
            <?php if(count($m1->childs)>0){ ?>
              <li class="">
                <a href="<?=$url?>" title="<?=$m1->nama?>" class="">
                  <?=$m1->nama?>
                </a>
                <ul class="">
                  <?php foreach($m1->childs as $m2){ ?>
                    <?php $url = $m2->url; if($m2->url_type == 'internal') $url = base_url($m2->url); ?>
                    <?php if($m2->url == '#') $url = '#'; ?>
                    <?php if(count($m2->childs)>0){ ?>
                      <li class="">
                        <a href="<?=$url?>">
                          <?=$m2->nama?>
                        </a>
                        <ul class="">
                          <?php foreach($m2->childs as $m3){ ?>
                            <?php $url = $m3->url; if($m3->url_type == 'internal') $url = base_url($m3->url); ?>
                            <?php if($m3->url == '#') $url = '#'; ?>
                            <li class="">
                              <a href="<?=$url?>">
                                <?=$m3->nama?>
                              </a>
                            </li>
                          <?php } ?>
                        </ul>
                      </li>
                    <?php }else{ ?>
                      <li class="">
                        <a href="<?=$url?>">
                          <?=$m2->nama?>
                        </a>
                      </li>
                    <?php } ?>
                  <?php } ?>
                </ul>
              </li>
            <?php }else{ ?>
              <li class="">
                <a href="<?=$url?>" title="Menuju <?=$m1->nama?>" class="" >
                  <?=$m1->nama?>
                </a>
              </li>
            <?php } ?>
          <?php }} ?>
        </ul>
      </div>

      <div class="col-md-4">
        <ul class="nav nav-pills nav-stacked">
          <?php if(isset($menu_footer_2)){ foreach($menu_footer_2 as $m1){ ?>
            <?php $url = $m1->url; if($m1->url_type == 'internal') $url = base_url($m1->url); ?>
            <?php if($m1->url == '#') $url = '#'; ?>
            <?php if(count($m1->childs)>0){ ?>
              <li class="">
                <a href="<?=$url?>" title="<?=$m1->nama?>" class="">
                  <?=$m1->nama?>
                </a>
                <ul class="">
                  <?php foreach($m1->childs as $m2){ ?>
                    <?php $url = $m2->url; if($m2->url_type == 'internal') $url = base_url($m2->url); ?>
                    <?php if($m2->url == '#') $url = '#'; ?>
                    <?php if(count($m2->childs)>0){ ?>
                      <li class="">
                        <a href="<?=$url?>">
                          <?=$m2->nama?>
                        </a>
                        <ul class="">
                          <?php foreach($m2->childs as $m3){ ?>
                            <?php $url = $m3->url; if($m3->url_type == 'internal') $url = base_url($m3->url); ?>
                            <?php if($m3->url == '#') $url = '#'; ?>
                            <li class="">
                              <a href="<?=$url?>">
                                <?=$m3->nama?>
                              </a>
                            </li>
                          <?php } ?>
                        </ul>
                      </li>
                    <?php }else{ ?>
                      <li class="">
                        <a href="<?=$url?>">
                          <?=$m2->nama?>
                        </a>
                      </li>
                    <?php } ?>
                  <?php } ?>
                </ul>
              </li>
            <?php }else{ ?>
              <li class="">
                <a href="<?=$url?>" title="Menuju <?=$m1->nama?>" class="" >
                  <?=$m1->nama?>
                </a>
              </li>
            <?php } ?>
          <?php }} ?>
        </ul>
      </div>

    </div>
  </div>
</div>
<footer class="footer-copyright">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <p class="copyright"><?=$this->site_name?> - <?=$this->site_tagline?> &copy; <?=date("Y")?> v-<?=$this->site_version?>.</p>
      </div>
    </div>
  </div>
</footer>
