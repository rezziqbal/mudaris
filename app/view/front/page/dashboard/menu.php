<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?=base_url()?>">
        <div class="seme-site-logo-wrapper">
          <img src="<?=$this->cdn_url("skin/front/images/logo.png")?>" class="seme-site-logo" />
          <?=strtoupper($this->site_name)?>
        </div>
      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="<?=base_url("dashboard")?>" title="Menuju Dashboard" class="" ><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-bank" aria-hidden="true"></i>
            Data Sekolah
            <i class="fa fa-angle-down"></i>
          </a>
          <ul class="dropdown-menu">
            <li><a href="<?=base_url("sekolah")?>" title="Kelola data sekolah" class="" >Sekolah</a></li>
            <li><a href="<?=base_url("kelas")?>" title="Kelola data kelas" class="" >Kelas</a></li>
            <li><a href="<?=base_url("matapelajaran")?>" title="Kelola data mata pelajaran" class="" >Mata Pelajaran</a></li>
            <li><a href="<?=base_url("siswa")?>" title="Kelola data siswa" class="" >Siswa</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-id-card-o" aria-hidden="true"></i>
            Ujian
            <i class="fa fa-angle-down"></i>
          </a>
          <ul class="dropdown-menu">
            <li><a href="<?=base_url("ujian")?>" title="Kelola data ujian dan soal" class="" >Ujian &amp; Soal</a></li>
            <li><a href="<?=base_url("pelaksanaan")?>" title="Kelola data pelaksanaan ujian" class="" >Pelaksanaan Ujian</a></li>
            <li><a href="<?=base_url("rekapujian")?>" title="Kelola data Hasil Ujian" class="" >Hasil Ujian</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-list"></i>
            Menu Lainnya
            <i class="fa fa-angle-down"></i>
          </a>
          <ul class="dropdown-menu">
            <li><a href="<?=base_url("profil")?>" title="Profil" class="" ><i class="fa fa-user" aria-hidden="true"></i> Profil Saya</a></li>
            <li><a href="<?=base_url("logout")?>" title="Keluar" class="" ><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
