<div class="row">
  <div class="col-md-3">&nbsp;</div>
  <div class="col-md-6">
    <h1 class="text-center">Login</h1>
    <div class="alert alert-info" role="alert">
      <span><i class="fa fa-info-circle"></i>
        <?php if(strlen($notif)>4){ echo $notif; }else{ ?>
        Silakan login sebelum dapat masuk ke Dashboard
        <?php } ?>
      </span>
    </div>
    <form id="flogin" method="post" action="<?=base_url("login")?>" enctype="multipart-multipart/form-data" class="form-horizontal">
      <div class="form-group">
        <div class="col-md-12">
          <label for="iemail" class="control-label">Email *</label>
          <input id="iemail" name="email" type="text" class="form-control" placeholder="Email ketika daftar, cth: emilia@gmail.com" required />
        </div>
        <div class="col-md-12" class="control-label">
          <label for="ipassword" class="control-label">Password *</label>
          <input id="ipassword" name="password" type="password" minlength="6" class="form-control" placeholder="Password ketika daftar" required />
        </div>
      </div>
      <div class="form-group">
        <div class="col-md-12" class="control-label">
          <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-key"></i> Login</button>
        </div>

        <div class="col-md-12">
          <br />
          <div class="">
            Tidak bisa login? <a href="<?=base_url("daftar")?>" class="cold-link">Daftar lagi</a> atau <a href="<?=base_url("lupa")?>" class="cold-link">Lupa Password</a>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
