<h1>Selamat datang di Mudaris</h1>
<p>
  Mudaris merupakan aplikasi untuk memudahkan siswa dan guru untuk melakukan tes, ujian, ulangan, quiz, UTS, UAS, dan ujian lainnya dengan menggunakan Handphone Android.
</p>
<p>
  Mudaris berasal dari bahasa Arab yang artinya Guru. Mudaris memiliki semboyan <i>Mudah dan Praktis</i> yang mana ketika siswa menjalani ujian sudah tidak usah ribet menggunakan kertas, fotocopy, dan penilaian secara langsung.
</p>
<h2>Bagaimana cara mendaftar di Mudaris?</h2>
<p>Cara daftar di Aplikasi terbagi menjadi dua bagian, antara lain:</p>
<ol>
  <li>
    Cara daftar untuk Guru
  </li>
  <li>
    Cara daftar untuk Siswa
  </li>
</ol>
<h3>Cara pendaftaran untuk Guru</h3>
<p>
  Berikut ini adalah langkah pendaftaran untuk Guru di Mudaris:
</p>
<ol>
  <li>
    Untuk melakukan pendaftaran guru cukup menekan tombol daftar dikanan atas halaman ini, atau ikuti <a href="<?=base_url("daftar")?>" target="_blank">link pendaftaran Mudaris</a>.
  </li>
  <li>Setelah itu isi dan lengkapi formulir pendaftaran dengan data yang sah.</li>
  <li>Kemudian periksa kembali isian formulir barangkali ada yang salah ketik.</li>
  <li>Jika sudah merasa benar dan lengkap, silakan tekan tombol daftar dibagian bawah formulir.</li>
  <li>Tunggu 1 sampai 60 menit, kami akan segera mengirimkan email verifikasi.</li>
  <li>Biasanya email akan langsung masuk, jika tidak ada coba periksa di Spam.</li>
  <li>Jika email dari kami sudah ada, silakan klik link yang ada didalam email untuk menyelesaikan pendaftaran.</li>
  <li>Jika ada kendala atau pertanyaan silakan langsung menghubungi kami melalui halaman kontak.</li>
</ol>
<h3>Cara pendaftaran untuk Siswa</h3>
<p>
  Pendaftaran untuk siswa dilakukan melalui aplikasi Mudaris. Beriku ini adalah langkah-langkahnya:
</p>
<ol>
  <li>Download aplikasi Mudaris di <a href="#">Playstore</a>.</li>
  <li>Setelah itu buka aplikasi, kemudian pilih Sekolah tekan tombol lanjutkan.</li>
  <li>Jika sekolahnya belum terdaftar, silakan hubungi guru yang bersangkutan untuk mendaftar terlebih dahulu.</li>
  <li>Kemudian isi nama lengkap dan pilih kelas.</li>
  <li>Jika sudah merasa benar dan lengkap, tekan tombol masuk.</li>
  <li>Jika ada ujian aktif, halaman tunggu ujian akan tampil.</li>
  <li>Jika tidak ada ujian aktif, maka akan muncul pesan tidak ada ujian.</li>
  <li>Jika ada kendala atau pertanyaan silakan langsung menghubungi guru yang bersangkutan untuk mendapatkan bantuan teknis dari Team Mudaris.</li>
</ol>
