
<div class="row halaman-judul-wrapper">
  <div class="col-md-8 halaman-judul">
    <h1>Data Siswa</h1>
    <p>
      Berikut ini adalah data siswa yang ada di <?=$sekolah->nama?>:
    </p>
  </div>
  <div class="col-md-4 halaman-aksi">
    <div class="btn-group pull-right">
      <a id="atambah" href="#" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</a>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">

    <div class="table-reponsive">
      <table id="drTable" class="table table-bordered">
        <thead>
          <tr>
            <th>ID</th>
            <th>Kelas</th>
            <th>No Urut</th>
            <th>Nama</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>

  </div>
</div>
