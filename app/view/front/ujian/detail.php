<style>
ul li.is_true {
  background-color: rgba(162,220,119,0.2);
}
.kartu-soal {
  background-color: #f5f5f5;
  padding: 1em;
  border-radius: 15px;
  margin-bottom: 15px;
}
</style>
<div class="row">
  <div class="col-md-12 text-center">
    <div class="pull-left">
      <div class="btn-group">
        <a href="<?=base_url("ujian")?>" class="btn btn-default"><i class="fa fa-chevron-left"></i> Kembali</a>
      </div>
    </div>
    <div class="pull-right">
      <div class="btn-group">
        <a id="atambah" href="#div_soal_input" class="btn btn-info"><i class="fa fa-plus"></i> Tambah Soal</a>
      </div>
    </div>
    <h1><?=$ujian->nama?></h1>
  </div>
  <div class="col-md-6">
    <h4>Mata Pelajaran: <?=$matapelajaran[$ujian->c_matapelajaran_id]->nama?></h4>
  </div>
  <div class="col-md-6 text-right">
    <h4><?php if($ujian->sifat != 'hide') echo 'Sifat: '.$this->__sifat($ujian->sifat); ?></h4>
  </div>
</div>
<div id="div_soal" class="row">
  <div class="col-md-12">
    <div class="well">
      <h4>Belum ada soal untuk ujian ini, <a id="awelltambah" href="#div_soal_input">tambahkan soal</a> ?</h4>
    </div>
  </div>
</div>

<div id="div_soal_input" class="row" style="display:none;">
  <div class="col-md-12">
    <form id="soal_input_form" action="" method="post" class="">
      <div class="row">
        <div class="col-md-12">
          <label for="isoal">Soal</label>
          <textarea id="isoal" name="soal" class="ckeditor"></textarea>
          <input type="hidden" name="d_ujian_id" value="<?=$ujian->id?>" />
        </div>
        <?php for($i=1;$i<=$ujian->jml_opsi;$i++){ ?>
        <div class="col-md-6">
          <br />
          <label for="ipilihan<?=$i?>" class="control-label">Pilihan Ke-<?=$i?></label>
          &nbsp;<label><input id="cbpilihanke<?=$i?>" type="checkbox" class="input-is-true" data-ke="<?=$i?>" /> Jawaban terpilih</label>
          <textarea id="ipilihan<?=$i?>" name="pilihan<?=$i?>" class="ckeditor<?=$i?>"></textarea>
          <input id="iis_true<?=$i?>" name="is_true<?=$i?>" type="hidden" value="0" class="hidden-is-true" />
        </div>
        <?php } ?>
        <div class="col-md-12">
          <br />
          <div class="btn-group pull-right">
            <button type="button" class="btn btn-default btn-soal-input-tutup"><i class="fa fa-times"></i> Tutup</button>
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
