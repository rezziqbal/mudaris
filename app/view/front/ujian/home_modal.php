<!-- modal pilihan -->
<div id="pilihan_modal" class="modal fade " role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header text-center">
				<h2 class="modal-title">Pilihan</h2>
			</div>
			<!-- END Modal Header -->

			<!-- Modal Body -->
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 btn-group-vertical">
						<a id="apelaksanaan" href="#" class="btn btn-info text-left" style="text-align: left;"><i class="fa fa-asterisk"></i> Pelaksanaan Ujian</a>
						<a id="aedit" href="#" class="btn btn-info text-left" style="text-align: left;"><i class="fa fa-pencil"></i> Edit Ujian</a>
						<a id="aedit_soal" href="#" class="btn btn-info text-left" style="text-align: left;"><i class="fa fa-list"></i> Edit Soal</a>
						<button id="bhapus" type="button" class="btn btn-danger text-left" style="text-align: left;"><i class="fa fa-trash-o"></i> Hapus</button>
					</div>
				</div>
				<div class="row" style="margin-top: 1em; ">
					<div class="col-md-12" style="border-top: 1px #afafaf dashed;">&nbsp;</div>
					<div class="col-xs-12 btn-group-vertical" style="">
						<button type="button" class="btn btn-default btn-block text-left" data-dismiss="modal"><i class="fa fa-times"></i> Tutup</button>
					</div>
				</div>
				<!-- END Modal Body -->
			</div>
		</div>
	</div>
</div>
<!-- modal pilihan -->

<!-- modal tambah -->
<div id="tambah_modal" class="modal fade " role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header text-center">
				<h2 class="modal-title">Tambah</h2>
			</div>
			<!-- END Modal Header -->

			<!-- Modal Body -->
			<div class="modal-body">
				<form id="tambah_modal_form" action="" method="post">
					<div class="row">
						<div class="col-md-4">
							<div class="form-wrapper">
								<label for="ic_matapelajaran_id" class="control-label">Mata Pelajaran*</label>
								<select id="ic_matapelajaran_id" name="c_matapelajaran_id" class="form-control">
									<option value="null">--Belum Memilih--</option>
									<?php if(isset($matapelajaran)){ foreach($matapelajaran as $mp){ ?>
									<option value="<?=$mp->id?>"><?=$mp->nama?></option>
									<?php }} ?>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-wrapper">
								<label for="isifat" class="control-label">Sifat</label>
								<select id="isifat" name="sifat" class="form-control">
									<option value="hide">Normal</option>
									<option value="open">Open Book</option>
									<option value="close">Closed Book</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-wrapper">
								<label for="inama" class="control-label">Nama Ujian*</label>
								<input id="inama" name="nama" type="text" class="form-control" placeholder="cth: Ulangan ke-1" required />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-wrapper">
								<label for="ijml_opsi" class="control-label">Jumlah Pilihan*</label>
								<input id="ijml_opsi" name="jml_opsi" type="number" class="form-control" placeholder="3" value="3" required />
							</div>
						</div>
					</div>
					<div class="row" style="margin-top: 1em; ">
						<div class="col-md-12" style="border-top: 1px #afafaf dashed;">&nbsp;</div>
						<div class="col-xs-12 " style="">
							<div class="btn-group pull-right">
								<button type="button" class="btn btn-default " data-dismiss="modal"><i class="fa fa-times"></i> Tutup</button>
								<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
							</div>
						</div>
					</div>

				</form>
			</div>	<!-- END Modal Body -->

		</div>
	</div>
</div>
<!-- modal tambah -->

<!-- modal edit -->
<div id="edit_modal" class="modal fade " role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header text-center">
				<h2 class="modal-title">Edit</h2>
			</div>
			<!-- END Modal Header -->

			<!-- Modal Body -->
			<div class="modal-body">
				<form id="edit_modal_form" action="" method="post">
					<div class="row">
						<div class="col-md-4">
							<div class="form-wrapper">
								<label for="iec_matapelajaran_id" class="control-label">Kelas*</label>
								<select id="iec_matapelajaran_id" name="c_matapelajaran_id" class="form-control">
									<option value="null">--Belum Memilih--</option>
									<?php if(isset($matapelajaran)){ foreach($matapelajaran as $mp){ ?>
									<option value="<?=$mp->id?>"><?=$mp->nama?></option>
									<?php }} ?>
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-wrapper">
								<label for="ienourut" class="control-label">No Urut</label>
								<input id="ienourut" name="nourut" type="number" class="form-control" placeholder="cth: 1" required />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-wrapper">
								<label for="iesifat" class="control-label">Sifat</label>
								<select id="iesifat" name="sifat" class="form-control">
									<option value="hide">Normal</option>
									<option value="open">Open Book</option>
									<option value="close">Closed Book</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-wrapper">
								<label for="ienama" class="control-label">Nama*</label>
								<input id="ienama" name="nama" type="text" class="form-control" placeholder="cth: Ulangan Minggu ke-1" required />
							</div>
						</div>
					</div>
					<div class="row">
            <div class="col-md-4">
							<div class="form-wrapper">
								<label for="iejml_opsi" class="control-label">Jumlah Pilihan*</label>
								<input id="iejml_opsi" name="jml_opsi" type="number" class="form-control" placeholder="3" value="3" required />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-wrapper">
								<label for="ieis_active" class="control-label">Status*</label>
								<select id="ieis_active" name="is_active" class="form-control">
									<option value="1">Aktif</option>
									<option value="0">Tidak Aktif</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row" style="margin-top: 1em; ">
						<div class="col-md-12" style="border-top: 1px #afafaf dashed;">&nbsp;</div>
						<div class="col-xs-12" style="">
							<div class=" btn-group pull-right">
								<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Tutup</button>
								<button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
							</div>
						</div>
					</div>

				</form>
			</div>	<!-- END Modal Body -->

		</div>
	</div>
</div>
<!-- modal tambah -->

<!-- modal pelaksanaan ujian baru -->
<div id="pelaksanaan_ujian_baru" class="modal fade " role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header text-center">
				<h2 class="modal-title">Tambah Pelaksanaan Ujian</h2>
			</div>
			<!-- END Modal Header -->

			<!-- Modal Body -->
			<div class="modal-body">
        <form id="pelaksanaan_ujian_baru_form" action="" method="post">
  				<div class="row">
  					<div class="col-xs-12 btn-group-vertical">
              <select id="" class="form-control">
                <?php if(isset($kelas)){ foreach($kelas as $kl){ ?>
                <option value="<?=$kl->id?>"><?=$kl->nama?></option>
                <?php } } ?>
              </select>
  					</div>
  				</div>
  				<div class="row" style="margin-top: 1em; ">
  					<div class="col-md-12" style="border-top: 1px #afafaf dashed;">&nbsp;</div>
  					<div class="col-md-12 " style="">
							<div class="btn-group pull-right">
								<button type="button" class="btn btn-default" style="text-align: left;" data-dismiss="modal"><i class="fa fa-times"></i> Tutup</button>
								<button type="submit" class="btn btn-primary" style="text-align: left;"><i class="fa fa-save"></i> Simpan</button>
							</div>
						</div>
  				</div>
        </form>
			</div> <!-- END Modal Body -->
		</div>
	</div>
</div>
<!-- modal pelaksanaan ujian baru -->
