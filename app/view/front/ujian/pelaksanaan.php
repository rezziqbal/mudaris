<div class="row halaman-judul-wrapper">
  <div class="col-md-8 halaman-judul">
    <h1>Pelaksanaan Ujian</h1>
  </div>
  <div class="col-md-4 halaman-aksi">
    <div class="btn-group pull-right">
      <a id="ahentikan" href="#" class="btn btn-danger"><i class="fa fa-stop"></i> Selesai</a>
      <a id="ajeda" href="#" class="btn btn-warning"><i class="fa fa-pause"></i> Jeda</a>
      <a id="amulai" href="#" class="btn btn-success"><i class="fa fa-play"></i> Mulai</a>
    </div>
  </div>
</div>
<div class="row halaman-judul-wrapper">
  <div class="col-md-5">
    <table class="table ">
      <tr>
        <td class="col-md-3">Nama Ujian</td>
        <td>:</td>
        <td><?=$ujian->nama?></td>
      </tr>
      <tr>
        <td>Mata Pelajaran</td>
        <td>:</td>
        <td><?=$matapelajaran[$ujian->c_matapelajaran_id]->nama?></td>
      </tr>
    </table>
  </div>
  <div class="col-md-2">&nbsp;</div>
  <div class="col-md-5">
    <table class="table ">
      <tr>
        <td class="col-md-3">Sifat</td>
        <td>:</td>
        <td><?=$this->__sifat($ujian->sifat)?></td>
      </tr>
      <tr>
        <td>Kelas</td>
        <td>:</td>
        <td id="td_kelas"><a id="a_pilih_kelas" href="#" class="btn btn-primary btn-xs"> Pilih Kelas</a></td>
      </tr>
    </table>
  </div>
</div>

<div class="row">
  <div class="col-md-12">

    <div class="table-reponsive">
      <table id="drTable" class="table table-bordered">
        <thead>
          <tr>
            <th>ID</th>
            <th>Mata Pelajaran</th>
            <th>Urutan</th>
            <th>Nama</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>

  </div>
</div>
