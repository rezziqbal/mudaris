var ieid = '';
var e_ujian_id = '<?=$ujian->id?>';
var soal_method = 'tambah';

function gritter(teks,jenis="info"){
  $.bootstrapGrowl(teks, {
    type: jenis,
    delay: 2500,
    allow_dismiss: true
  });
};

CKEDITOR.replace('isoal');
<?php for($i=1;$i<=$ujian->jml_opsi;$i++){ ?>
CKEDITOR.replace('ipilihan<?=$i?>');
<?php } ?>

$("#atambah").on("click",function(e){
  e.preventDefault();
  $("#soal_input_form").trigger("reset");
  soal_method = 'tambah';
  $("#div_soal_input").toggle("slow");
  $.scrollTo($("#div_soal_input"),800);
});

$("#awelltambah").on("click",function(e){
  e.preventDefault();
  $("#atambah").trigger("click");
});
$(".btn-soal-input-tutup").on("click",function(e){
  e.preventDefault();
  $("#atambah").trigger("click");
})

$("#soal_input_form").on("submit",function(e){
  e.preventDefault();
  if($(".input-is-true:checked").length==0){
    gritter("<h4>Perhatian</h4><p>Anda belum memilih salah satu jawaban yang benar</p>","warning");
    return false;
  }
  NProgress.start();
  for ( instance in CKEDITOR.instances ) CKEDITOR.instances[instance].updateElement();
  var fd = new FormData($(this)[0]);
  var url = '<?=base_url('api_front/soal/tambah/'); ?>';
  if(soal_method=='edit') url = '<?=base_url('api_front/soal/edit/'); ?>'+encodeURIComponent(ieid);
  $.ajax({
    url: url,
		type: "POST",
		data: fd,
		contentType: false,
		cache: false,
		processData:false,
		success: function(data){
			NProgress.done();
      $("#tambah_modal").modal("hide");
			if(data.status == 200){
        gritter('<h4>Berhasil</h4><p>Data baru berhasil ditambahkan</p>','success');
				getSoal();
        $.scrollTo($("#div_soal"),800);
        $(this).trigger("reset");
        $("#div_soal_input").toggle("slow");
			}else{
				gritter('<h4>Gagal</h4><p>'+data.message+'</p>','danger');
			}
		},
		error: function(data){
			NProgress.done();
			gritter('<h4>Error</h4><p>Saat ini proses penambahan data sedang error, coba lagi nanti</p>','warning');
		}
  });
});

$(".input-is-true").on("change",function(e){
  e.preventDefault();
  $(".hidden-is-true").val('0');
  var ke = $(this).attr("data-ke");
  $(".input-is-true").not(this).prop('checked', false);
  $("#iis_true"+ke).val('1');
});

function getSoal(){
  NProgress.start();
  $.get('<?=base_url("api_front/soal/index/".$ujian->id.'/')?>').done(function(dt){
    NProgress.done();
    if(dt.status == 200){
      var h = '<div class="col-md-12"> <div class="well"> <h4>Belum ada soal untuk ujian ini, <a id="awelltambah" href="#div_soal_input">tambahkan soal</a> ?</h4></div></div>';
      if(dt.data.length){
        var h = '';
        var i = 1;
        $.each(dt.data,function(k,v){
          h += '<div class="col-md-6">';
          h += '<div class="kartu-soal">';
          h += '<div class="pull-right">';
          h += '<div class="btn-group">';
          h += '<a href="#" class="btn btn-info btn-sm btn-soal-edit" data-id="'+v.id+'"><i class="fa fa-edit"></i></a>';
          h += '<a href="#" class="btn btn-danger btn-sm btn-soal-hapus" data-id="'+v.id+'"><i class="fa fa-trash-o"></i></a>';
          h += '</div>';
          h += '</div>';
          h += '<div class="std"><span class="pull-left" style="margin-right: 10px;">'+i+'.</span> '+v.soal+'</div>';
          h += '<ul>';
          $.each(v.pilihan,function(kp,vp){
            var it = '';
            if(vp.is_true == "1") it="is_true";
            h += '<li class="'+it+'">';
            h += '<div class="std">'+vp.pilihan+'</div>';
            h += '</li>';
          });
          h += '</ul>';
          h += '</div>';
          h += '</div>';
          i++;
        });
      }
      $("#div_soal").html(h);
    }else{
      gritter("<h4>Gagal</h4><p>"+dt.message+"</p>",'warning');
    }
  }).fail(function(){
    NProgress.done();
    gritter("<h4>Error</h4><p>Tidak dapat mengambil data soal saat ini, coba lagi nanti</p>",'warning');
  })
}

NProgress.start();
setTimeout(function(){
  getSoal();
},1000);
