<h1>Data Sekolah</h1>
<p>
  Berikut ini adalah data sekolah tempat anda mendaftar
</p>
<div class="row">
  <div class="col-md-6">
    <form method="post" action="<?=base_url("sekolah/")?>" class="">
      <div class="form-group">
        <?php if(isset($notif)){ ?>
        <div class="col-md-12">
          <div class="alert alert-info">
            <?=$notif?>
          </div>
        </div>
        <?php } ?>
        <div class="col-md-12">
          <label for="inama" class="control-label">Nama Sekolah *</label>
          <input id="inama" name="nama" type="text" class="form-control" value="<?=strtoupper($sekolah->nama)?>" disabled />
        </div>
        <div class="col-md-6">
          <label for="itingkat" class="control-label">Tingkat *</label>
          <select name="tingkat" class="form-control" disabled required>
            <option value="atas" <?php if($sekolah->tingkat=='atas') echo 'selected'; ?>>SMA / SMK</option>
            <option value="menengah" <?php if($sekolah->tingkat=='menengah') echo 'selected'; ?>>SMP</option>
            <option value="dasar" <?php if($sekolah->tingkat=='adasartas') echo 'selected'; ?>>SD</option>
          </select>
        </div>
        <div class="col-md-6">
          <label for="ijenis" class="control-label">Jenis *</label>
          <select id="ijenis" name="jenis" class="form-control" disabled required>
            <option value="umum" <?php if($sekolah->jenis=='umum') echo 'selected'; ?>>Umum</option>
            <option value="kejuruan" <?php if($sekolah->jenis=='kejuruan') echo 'selected'; ?>>Kejuruan</option>
          </select>
        </div>
      </div>
      <div class="form-group"><div class="col-md-12"><br /></div></div>
      <div class="form-group">
        <div class="col-md-12">
          <label for="inama" class="control-label">Alamat *</label>
          <input id="inama" name="alamat" type="text" class="form-control" value="<?=ucwords($sekolah->alamat)?>" />
        </div>
        <div class="col-md-6">
          <label for="ikabkota" class="control-label">Kab / Kota *</label>
          <input id="ikabkota" name="kabkota" type="text" class="form-control" value="<?=ucwords($sekolah->kabkota)?>" />
        </div>
        <div class="col-md-6">
          <label for="iprovinsi" class="control-label">Provinsi *</label>
          <input id="iprovinsi" name="provinsi" type="text" class="form-control" value="<?=ucwords($sekolah->provinsi)?>" />
        </div>
      </div>
      <div class="form-action">
        <div class="col-md-12">
          <br />
          <div class="btn-group">
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan Perubahan</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
