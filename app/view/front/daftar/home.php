<div class="row">
  <div class="col-md-3">&nbsp;</div>
  <div class="col-md-6">
    <h1 class="text-center">Formulir Pendaftaran</h1>
    <div class="alert alert-info" role="alert">
      <span><i class="fa fa-info-circle"></i>
        <?php if(strlen($notif)>4){ echo $notif; }else{ ?>
        Silakan lengkapi formulir pendaftaran dengan sebaik-baiknya
        <?php } ?>
        </span>
    </div>
    <form id="fdaftar" method="post" action="<?=base_url("daftar")?>" enctype="multipart-multipart/form-data" class="form-horizontal">
      <div class="form-group">
        <div class="col-md-12">
          <label for="inama" class="control-label">Nama Lengkap*</label>
          <input id="inama" name="nama" type="text" class="form-control" placeholder="Emilia Contoso, S.Pd" required />
        </div>
        <div class="col-md-12">
          <label for="iemail" class="control-label">Email *</label>
          <input id="iemail" name="email" type="text" class="form-control" placeholder="emilia@gmail.com" required />
        </div>
        <div class="col-md-12" class="control-label">
          <label for="ipassword" class="control-label">Password *</label>
          <input id="ipassword" name="password" type="password" minlength="6" class="form-control" placeholder="Mudah diingat, Minimal 6 huruf atau angka" required />
        </div>
        <div class="col-md-12" class="control-label">
          <label for="iulangi_password" class="control-label">Konfirmasi Password *</label>
          <input id="iulangi_password" name="ulangi_password" type="password" minlength="6" class="form-control" placeholder="Ulangi password yang sama sekali lagi" required />
        </div>
        <div class="col-md-12" class="control-label">
          <label for="isekolah" class="control-label">Nama Lengkap Sekolah *</label>
          <input id="isekolah" name="sekolah" type="text" class="form-control" placeholder="SMAN 1 Soreang" required />
        </div>
        <div class="col-md-12" class="control-label">
          <label for="imatapelajaran" class="control-label">Mata Pelajaran *</label>
          <input id="imatapelajaran" name="matapelajaran" type="text" class="form-control" placeholder="Bahasa Indonesia" required />
        </div>
      </div>
      <div class="form-group">
        <div class="col-md-12" class="control-label">
          <div class="well">
            Dengan menekan tombol daftar anda berarti setuju dengan segala jenis layanan dan pengelolaan data privasi oleh Mudaris dan pemiliknya
          </div>
          <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-save"></i> Daftar</button>
        </div>

        <div class="col-md-12">
          <br />
          <div class="">
            Sudah pernah daftar? <a href="<?=base_url("login")?>" class="cold-link">Silakan Login Saja</a>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
