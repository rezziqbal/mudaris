<div class="row">
  <div class="col-md-3">&nbsp;</div>
  <div class="col-md-6">
    <h1 class="text-center">Lupa Password</h1>
    <div class="alert alert-info" role="alert">
      <span><i class="fa fa-info-circle"></i>
        <?php if(strlen($notif)>4){ echo $notif; }else{ ?>
        Silakan masukan email yang digunakan ketika daftar, cara mengganti password akan kita kirim melalui email tersebut.
        <?php } ?>
      </span>
    </div>
    <form id="flupa" method="post" action="<?=base_url("lupa")?>" enctype="multipart-multipart/form-data" class="form-horizontal">
      <div class="form-group">
        <div class="col-md-12">
          <label for="iemail" class="control-label">Email *</label>
          <input id="iemail" name="email" type="text" class="form-control" placeholder="Email ketika daftar, cth: emilia@gmail.com" required />
        </div>
      </div>
      <div class="form-group">
        <div class="col-md-12" class="control-label">
          <button type="submit" class="btn btn-primary btn-alt btn-block"><i class="fa fa-envelope"></i> Kirim Email </button>
        </div>

        <div class="col-md-12">
          <br />
          <div class="">
            <a href="<?=base_url("daftar")?>" class="cold-link">Daftar Saja</a>
            <a href="<?=base_url("login")?>" class="cold-link">Login lagi</a>
          </div>
        </div>

      </div>
    </form>
  </div>
</div>
