var ieid = '';
var drTable = {};

function gritter(teks,jenis="info"){
  $.bootstrapGrowl(teks, {
    type: jenis,
    delay: 2500,
    allow_dismiss: true
  });
};

if(jQuery('#drTable').length>0){
	drTable = jQuery('#drTable')
	.on('preXhr.dt', function ( e, settings, data ){
    NProgress.start();
	}).DataTable({
			"order"					: [[ 1, "asc" ]],
			"responsive"	  : true,
			"bProcessing"		: true,
			"bServerSide"		: true,
			"sAjaxSource"		: "<?=base_url("api_front/matapelajaran/"); ?>",
			"fnServerData"	: function (sSource, aoData, fnCallback, oSettings) {
				oSettings.jqXHR = $.ajax({
					dataType 	: 'json',
					method 		: 'POST',
					url 		: sSource,
					data 		: aoData
				}).success(function (response, status, headers, config) {
					console.log(response);
          NProgress.done();
					$('#drTable > tbody').off('click', 'tr');
					$('#drTable > tbody').on('click', 'tr', function (e) {
						e.preventDefault();
						var id = $(this).find("td").html();
						ieid = id;
						var url = '<?=base_url("api_front/matapelajaran/detail/")?>'+ieid;
						$.get(url).done(function(response){
							if(response.status==200){
								var dta = response.data;
								//input nilai awal
                $("#ienama").val(dta.nama);
                $("#ieis_active").val(dta.is_active);

								//tampilkan modal
								$("#pilihan_modal").modal("show");
							}else{
								gritter('<h4>Galat</h4><p>'+response.message+'</p>','danger');
							}

						});
					});
					fnCallback(response);
				}).fail(function (response, status, headers, config) {
          NProgress.done();
					gritter('<h4>Galat</h4><p>Tidak dapat mengambil data</p>','warning');
				});
			},
	});
	$('.dataTables_filter input').attr('placeholder', 'Cari');
}

$("#atambah").on("click",function(e){
  e.preventDefault();
  $("#tambah_modal_form").trigger("reset");
  $("#tambah_modal").modal("show");
});
$("#tambah_modal_form").on("submit",function(e){
  e.preventDefault();
  NProgress.start();
  var fd = new FormData($(this)[0]);
  $.ajax({
    url: '<?=base_url('api_front/matapelajaran/tambah/'); ?>',
		type: "POST",
		data: fd,
		contentType: false,
		cache: false,
		processData:false,
		success: function(data){
			NProgress.done();
      $("#tambah_modal").modal("hide");
			if(data.status == 200){
        gritter('<h4>Berhasil</h4><p>Data baru berhasil ditambahkan</p>','success');
				drTable.ajax.reload(null,false);
			}else{
				gritter('<h4>Gagal</h4><p>'+data.message+'</p>','danger');
			}
		},
		error: function(data){
			NProgress.done();
			gritter('<h4>Error</h4><p>Saat ini proses penambahan data sedang error, coba lagi nanti</p>','warning');
		}
  });
});

$("#aedit").on("click",function(e){
  NProgress.start();
  $("#pilihan_modal").modal("hide");
  NProgress.set(0.7);
  setTimeout(function(){
    NProgress.done();
    $("#edit_modal").modal("show");
  },666);
});
$("#edit_modal_form").on("submit",function(e){
  e.preventDefault();
  NProgress.start();
  var fd = new FormData($(this)[0]);
  $.ajax({
    url: '<?=base_url('api_front/matapelajaran/edit/'); ?>'+encodeURIComponent(ieid),
		type: "POST",
		data: fd,
		contentType: false,
		cache: false,
		processData:false,
		success: function(data){
			NProgress.done();
      $("#edit_modal").modal("hide");
			if(data.status == 200){
        gritter('<h4>Berhasil</h4><p>Data berhasil diubah</p>','success');
				drTable.ajax.reload(null,false);
			}else{
				gritter('<h4>Gagal</h4><p>'+data.message+'</p>','danger');
			}
		},
		error: function(data){
			NProgress.done();
			gritter('<h4>Error</h4><p>Saat ini proses perubahan data sedang error, coba lagi nanti</p>','warning');
		}
  });
});

$("#bhapus").on("click",function(e){
  e.preventDefault();
  if(ieid){
    var c = confirm('Apakah anda yakin?');
    if(c){
			NProgress.start();
      $.get("<?=base_url("api_front/matapelajaran/hapus/")?>"+encodeURIComponent(ieid)).done(function(dt){
  			NProgress.done();
        $("#pilihan_modal").modal("hide");
        if(dt.status==100){
          gritter('<h4>Berhasil</h4><p>Data berhasil dihapus</p>','success');
          drTable.ajax.reload(null,false);
        }else{
          gritter('<h4>Gagal</h4><p>'+data.message+'</p>','danger');
        }
      }).fail(function(){
        NProgress.done();
  			gritter('<h4>Error</h4><p>Saat ini proses penghapusan data sedang error, coba lagi nanti</p>','warning');
  		});
    }
  }
});
