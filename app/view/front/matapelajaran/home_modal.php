<!-- modal pilihan -->
<div id="pilihan_modal" class="modal fade " role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header text-center">
				<h2 class="modal-title">Pilihan</h2>
			</div>
			<!-- END Modal Header -->

			<!-- Modal Body -->
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 btn-group-vertical">
						<a id="adetail" href="#" class="btn btn-info text-left" style="text-align: left;"><i class="fa fa-info-circle"></i> Detail</a>
						<a id="aedit" href="#" class="btn btn-info text-left" style="text-align: left;"><i class="fa fa-pencil"></i> Edit</a>
						<button id="bhapus" type="button" class="btn btn-danger text-left" style="text-align: left;"><i class="fa fa-trash-o"></i> Hapus</button>
					</div>
				</div>
				<div class="row" style="margin-top: 1em; ">
					<div class="col-md-12" style="border-top: 1px #afafaf dashed;">&nbsp;</div>
					<div class="col-xs-12 btn-group-vertical" style="">
						<button type="button" class="btn btn-default btn-block text-left" data-dismiss="modal"><i class="fa fa-times"></i> Tutup</button>
					</div>
				</div>
				<!-- END Modal Body -->
			</div>
		</div>
	</div>
</div>
<!-- modal pilihan -->

<!-- modal tambah -->
<div id="tambah_modal" class="modal fade " role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header text-center">
				<h2 class="modal-title">Tambah</h2>
			</div>
			<!-- END Modal Header -->

			<!-- Modal Body -->
			<div class="modal-body">
				<form id="tambah_modal_form" action="" method="post">
					<div class="row">
						<div class="col-md-12">
							<div class="form-wrapper">
								<label for="inama" class="control-label">Nama Mata Pelajaran*</label>
								<input id="inama" name="nama" type="text" class="form-control" placeholder="cth: XI IPA 2" required />
							</div>
						</div>
					</div>
					<div class="row" style="margin-top: 1em; ">
						<div class="col-md-12" style="border-top: 1px #afafaf dashed;">&nbsp;</div>
						<div class="col-xs-12 btn-group-vertical" style="">
							<button type="submit" class="btn btn-success btn-block"><i class="fa fa-save"></i> Simpan</button>
							<button type="button" class="btn btn-default btn-block text-left" data-dismiss="modal"><i class="fa fa-times"></i> Tutup</button>
						</div>
					</div>

				</form>
			</div>	<!-- END Modal Body -->

		</div>
	</div>
</div>
<!-- modal tambah -->

<!-- modal edit -->
<div id="edit_modal" class="modal fade " role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">

			<!-- Modal Header -->
			<div class="modal-header text-center">
				<h2 class="modal-title">Edit</h2>
			</div>
			<!-- END Modal Header -->

			<!-- Modal Body -->
			<div class="modal-body">
				<form id="edit_modal_form" action="" method="post">
					<div class="row">
						<div class="col-md-12">
							<div class="form-wrapper">
								<label for="ienama" class="control-label">Nama Mata Pelajaran*</label>
								<input id="ienama" name="nama" type="text" class="form-control" placeholder="cth: Bahasa Indonesia" required />
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-wrapper">
								<label for="ieis_active" class="control-label">Status*</label>
								<select id="ieis_active" name="is_active" class="form-control">
									<option value="1">Aktif</option>
									<option value="0">Tidak Aktif</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row" style="margin-top: 1em; ">
						<div class="col-md-12" style="border-top: 1px #afafaf dashed;">&nbsp;</div>
						<div class="col-xs-12 btn-group-vertical" style="">
							<button type="submit" class="btn btn-success btn-block"><i class="fa fa-save"></i> Simpan</button>
							<button type="button" class="btn btn-default btn-block text-left" data-dismiss="modal"><i class="fa fa-times"></i> Tutup</button>
						</div>
					</div>

				</form>
			</div>	<!-- END Modal Body -->

		</div>
	</div>
</div>
<!-- modal tambah -->
