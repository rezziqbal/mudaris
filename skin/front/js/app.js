document.gritter = function(teks,jenis="info"){
  jenis = jenis.toLowerCase();
  if(jenis=="danger"){
    $.growl.error({message: teks});
  }else if(jenis=='success'){
    $.growl.notice({message: teks});
  }else if(jenis=='warning'){
    $.growl.warning({message: "The kitten is ugly!"});
  }else{
    $.growl({ message: teks });
  }
};
